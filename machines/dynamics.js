(function($){

	$(document).ready(function(){

		webApp = new boilerplate('webApp');

		if(!webApp.ajaxer){
			webApp.start({
				singlePage: false,
				defaultPage: 'home',
				RewriteBase: "/",
				themeFolderName: 'spicer',
				typekitID: 'eft5dzv',
				filesToVariablesArray: [
					{'page_inner': 'views/page_inner.php'},
					{'page_home': 'views/page_home.php'},
					{'page_about': 'views/page_about.php'},
					{'blog_item': 'views/output_blog_item.php'},
					{'home_slide_item': 'views/output_home_slide_item.php'},
					{'testimonial_item': 'views/output_testimonial_item.php'},
					{'link_item': 'views/output_link_item.php'},
					{'form': 'views/output_form.php'},
					{'page_contact': 'views/page_contact.php'},
					{'qualifications_view': 'views/output_qualifications_view.php'},
					{'qualifications_item': 'views/output_qualifications_item.php'},
					{'homepageBox_item': 'views/output_homepage_boxes_item.php'}
				],
				viewRender: {
					"contact": function(thisThat){
						var that = thisThat;
						that.returnAndSaveJsonData('listPages', function(pagesData){
							thisPageData = that.accessor_listPages[pageID];
							var returnObj = that.renderModel(thisPageData, $(that.php_page_contact));
							$('.pageSection').html(returnObj);
							that.renderContactForm($('.contactFormBlock'));
							that.render_featured_image($('.featured_img'));
							that.changePage();
						});
					},
					"about": function(thisThat){
						var that = thisThat;
						that.returnAndSaveJsonData('listPages', function(pagesData){
							thisPageData = that.accessor_listPages[pageID];
							var pageObject = that.renderModel(thisPageData, $(that.php_page_about));
							$('.pageSection').html(pageObject);
							pageObject.find('.about_img').backstretch('http://www.spicerh2hinspections.com/wp-content/uploads/2016/01/action-shot-for-web-site-smallr.jpg');
							that.getQualifications($('.qualifications_container'));
							that.render_featured_image($('.featured_img'));
							that.changePage();
						});
					},				
					"home": function(thisThat){
						var that = thisThat;
						that.returnAndSaveJsonData('listPages', function(pagesData){
							thisPageData = that.accessor_listPages[pageID];
							$('.pageSection').html(that.renderModel(thisPageData, $(that.php_page_home)));
							that.renderHomeCarousel($('.homeSlides'));
							that.renderLinksListing($('.linkListing'));
							that.renderTestimonials($('.carouselContainer'));
							that.renderHomepageBoxes($('.homeBoxes'));
							that.changePage();
						});
					},
					"default": function(thisThat){
						var that = thisThat;
						that.returnAndSaveJsonData('listPages', function(pagesData){
							thisPageData = that.accessor_listPages[pageID];
							var returnObj = that.renderModel(thisPageData, $(that.php_page_inner));
							$('.pageSection').html(returnObj);
							that.render_featured_image($('.featured_img'));
							that.changePage();
						});
					}
					// FOR BLOG RENDERING
					// "blog": function(thisThat){
					// 	var that = thisThat;
					// 	that.renderBlog();
					// }
					// FOR SINGLE PAGE RENDERING
					// "default": function(thisThat, pageIDrequest){
					// 	var that = thisThat;
					// 	that.returnAndSaveJsonData('listPages', function(pagesData){
					// 		thisPageData = that.accessor_listPages[pageIDrequest];
					// 		pageObject = that.renderModel(thisPageData, $(that.php_page_inner));
					// 		pageObject.attr('id', 'page_' + pageIDrequest)
					// 		$('.pageSection').append(pageObject);
					// 	});
					// }
				},
				helpers: {
					getQualifications: function(target){
						var that 		  = this;
						var data 		  = {};
						var views 		  = {
							'qualifications_view': that.php_qualifications_view,
							'qualifications_item': that.php_qualifications_item
						};
						var targetElement = target;
						var init = function(){
							getQualificationsData(printQualificationsData);
						};
						var getQualificationsData = function(callback){
							that.returnAndSaveJsonData('listQualifications', function(d){
								data.qualifications = d;
								callback();
							});
						};
						var printQualificationsData = function(){
							var  qualifications_list = data.qualifications;
							var  qualifications_item = views.qualifications_item;
							var  returnObj 			 = $(views.qualifications_view);
							$.each(qualifications_list, function(index, value){
								var list_item = $(qualifications_item);
								list_item.find('.the_title').html(_.unescape(value.the_title));
								returnObj.append(list_item);
							});
							targetElement.append(returnObj);

							$('.qualifications').columnize({columns: 2, lastNeverTallest: true})
						};
						init();
					},
					renderContactForm: function(target){
						var that = this;
						var formReturnObject = $(that.php_form);
						formReturnObject.find('form').attr('id', 'contactForm');
						target.append(formReturnObject)
						that.processForm($('#contactForm'))
					},
				// SAVE PAGE FOR WEB CRAWLER
					 saveAjax: function() {
					 	var that = this;
						newDOM = $('html').clone();
						$.post(that.pageDir + "/machines/handlers/saveAjax.php", { pageID: pageID, postID: postID, html: newDOM[0].outerHTML })
						.done(function(data) {
						});
					},	
//mutlipagesaveajax
 multiPageSaveAjax: function(){
	var that = this;
	var recursiveSEO  = function () {
		var page_array_index = 0;
		var time_to_wait_on_page = 5000;

		var initialize = function (this_list_of_pages) {
			window['list_of_pages'] = this_list_of_pages;
			recursive_page_change()
		}

		var recursive_page_change = function(){
			// log the current number until it reaches our logic constraint
				var this_page_item = window['list_of_pages'][page_array_index];
				var newPostID = (this_page_item.postID != null) ? this_page_item.postID : "";
				that.pushPageNav(this_page_item.pageID, newPostID);

			// increment the default number
				page_array_index++;


			setTimeout(function(){
				// check if the test number is less than 100 AND less then our array length
					if(page_array_index < window['list_of_pages'].length){
						console.log(this_page_item)
						that.saveAjax();
						recursive_page_change();
					}
			}, time_to_wait_on_page);
					
				
		}

		var debug0 = function(value){
			console.log(value);
			$('#container').html(value);
		}

		return {
			initialize: initialize
		}
	}


					that.returnAndSaveJsonData('listPages', function (pagesData) {
						var pagesWithPostIds = [];
						var finalArray = [];
						_.each(pagesData, function (value_pagesData, index_pagesData) {
							var thisPageSlug = that.slugify(_.unescape(value_pagesData.the_title));

							if($.inArray(thisPageSlug, pagesWithPostIds) == -1){
								// if this page does not have post IDs
									var thisObject = {};
									thisObject['pageID'] = thisPageSlug;
									thisObject['postID'] = null;
									finalArray.push(thisObject);
							} else {
								var thisObject = {};
								thisObject['pageID'] = thisPageSlug;
								thisObject['postID'] = null;
								finalArray.push(thisObject);

								// switch(thisPageSlug){
								// 	case 'team':
								// 		_.each(peopleData, function(value, inde){
								// 			var this_post_id_slug = slugify(_.unescape(value.the_title))
								// 			var thisObject = {};
								// 			thisObject['pageID'] = thisPageSlug;
								// 			thisObject['postID'] = this_post_id_slug;
								// 			finalArray.push(thisObject);
								// 		})
								// 	break;

								// 	case 'portfolio':
								// 		_.each(propertyCategoriesData, function(value, inde){
								// 			var this_post_id_slug = slugify(_.unescape(value.the_title))
								// 			var thisObject = {};
								// 			thisObject['pageID'] = thisPageSlug;
								// 			thisObject['postID'] = this_post_id_slug;
								// 			finalArray.push(thisObject);
								// 		})
								// 		_.each(propertiesData, function(value, inde){
								// 			var this_post_id_slug = slugify(_.unescape(value.the_title))
								// 			var thisObject = {};
								// 			thisObject['pageID'] = thisPageSlug;
								// 			thisObject['postID'] = this_post_id_slug;
								// 			finalArray.push(thisObject);
								// 		})
								// 	break;


								// }

							}
						})

//uncomment each part as necessary (3 parts) 1 is debug, 2 is saveajax pages 3 is generate sitemap

						// _.each(finalArray, function(value_finalArray, index_finalArray){
						// 	console.log(value_finalArray)
						// })

						var this_seo_instance = new recursiveSEO();
						this_seo_instance.initialize(finalArray)

						// var xml_output = '';
						// _.each(finalArray, function(value_finalArray, index_finalArray){
						// 	var pageString = "";
						// 	if(value_finalArray.postID == null){
						// 		pageString = value_finalArray.pageID + '/';
						// 	} else {
						// 		pageString = value_finalArray.pageID + '/' + value_finalArray.postID + '/';
						// 	}
						// 	var this_xml_item = '<url><loc>http://www.spicerh2hinspections.com/' + pageString + '</loc><changefreq>weekly</changefreq></url>';
						// 	xml_output += this_xml_item;
						// })
						// console.log(xml_output)
						

					})





},										
					processForm: function(theForm, doCaptcha){
						var that = this;
						doCaptcha = (typeof doCaptcha !== "undefined") ? doCaptcha : false;
						csvUpdate = (theForm.data('csvupdate') != "") ? theForm.data('csvupdate') : 'formData';

						// you must enter your public key here - https://www.google.com/recaptcha/
						if(doCaptcha){
							Recaptcha.create("6LcgYfASAAAAAAvTvfb5r97NKTNnMbnCUnVN2-bp", theForm.find('.captchaField').attr('id'), {
								tabindex: 1,
								theme: "clean",
								callback: Recaptcha.focus_response_field
							});
						}

						function sendForm(){
							$.post(that.pageDir + "/machines/handlers/processForm.php", { formData: formData, csvUpdate: csvUpdate }, function(data) {
								if(data == "success"){
									// Shadowbox.teardown(); 
									// Shadowbox.init();

									// // to add a message that can be customized on backend, change defaultPage below to match the page slug
									// returnPageData(defaultPage).done(function(data1){
									// 	console.log(data1)
									// 	Shadowbox.open({
									// 		content: data1,
									// 		player: "html",
									// 		initialHeight: 500,
									// 		initialWidth: 500,
									// 		options: {onFinish: function () {
									// 			$('#sb-player').html("Thanks for Signing Up!")
									// 			$('#sb-player').append("<p class='printThis'>Print This</p>");
									// 			$('#sb-player').append("<p>title: " + theForm.find('.title').val() + "</p>");
									// 			$('#sb-player').append("<p>Telephone: " + theForm.find('.telephone').val() + "</p>");
									// 			$('#sb-player').append("<p>E-Mail: " + theForm.find('.email').val() + "</p>");
									// 			$('#sb-player').append("<p>Sample Select: " + theForm.find('.sample_select').val() + "</p>");

									// 			$('.printThis').on('click', function(){
									// 				window.print();
									// 			});
									// 		}} 
									// 	});
									// });

									$(".formResponse").html("Form sent!");
									// $(".formResponse").css('color', 'green');
									$(".formResponse").css('display', 'block !important');
										//$('#contactCaptcha').find('.fieldResponse').html('');
								} else {
									$(".formResponse").html("Form not sent. Please refresh the page and try again");
										//$(".formResponse").css('color', 'red');
									$(".formResponse").css('display', 'block !important');
								}
							});
						}

						theForm.validate({
							errorClass: "formError",
							submitHandler: function(thisForm) {
								formData = theForm.serialize();
								postArgs = {
									challenge: $('#recaptcha_challenge_field').val(),
									response: $('#recaptcha_response_field').val()				
								}

								// you must enter your private key in "/machines/libraries/recaptcha/recaptchaResponse.php" - https://www.google.com/recaptcha/
								if(doCaptcha){
									$.post(pageDir + "/machines/libraries/recaptcha/recaptchaResponse.php", postArgs, function(data){
										if(data.substring(0,4) == "true"){
											sendForm();
										} else {
											Recaptcha.reload()
											returnObject = $('<div class="fieldResponse">Incorrect Captcha, please try again</div>');
											$('#contactCaptcha').append(returnObject)
										};
									})
								} else {
									sendForm();
								}

							},
							invalidHandler: function(event, validator) {
								errors = validator.numberOfInvalids();
								if (errors) {
									message = errors == 1 ? 'You missed 1 field.' : 'You missed ' + errors + ' fields.';
									//$(".formResponse").html(message);
									$(".formResponse").show();
								} else {
									$(".formResponse").hide();
								}
							},
							errorPlacement: function(error, element) {
								switch(element.attr("name")){
									case "spam":
										$("input[name='spam']").parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
									break;
									case "transportation":
										$("input[name='transportation']").parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
									break;
									case "subscribe_email":
										element.parents('fieldset').after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
									break;
									default:
										element.parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
								}
							},
							messages: {
								email: {
									email: "Your email address must be in the format of name@domain.com"
								},
								telephone: {
									phoneUS: "Your phone number must be in the format of 212-555-1000"
								},
							},
							rules: {
								name: {
									required: true,
								},
								email: {
									required: true,
									email: true
								},
								telephone:{
									required: false,
									phoneUS: true
								},
								title:{
									required: false
								},
								subscribe_email:{
									required: true,
									email: true
								}
							}
						});
					},
					renderContactMap: function(contact_target_string){
						var that = this;
						// load Google Map
						var contactMap = new Maplace({
							map_div: contact_target_string,
							locations: [{
								lat: 40.60487,
								lon: -74.01547,
								zoom: 16
							}],
							controls_on_map: false
						})
						contactMap.Load();
					},
					renderTestimonials: function (target){
						var that = this;

						var views = {
							'testimonial_item': that.php_testimonial_item
						}

						var get_model_data = function (callback) {
							that.returnAndSaveJsonData('listTestimonials', function(){
								callback();
							});
						};

						var build_dom = function(){
							target.append('<div class="owl-carousel" />')
							var testimonialsData = that.jsonDataCollection['listTestimonials'];
							_.each(testimonialsData, function(value_testimonialsData, index_testimonialsData){
								var some_var = that.renderModel(value_testimonialsData, $(views.testimonial_item))
								target.find('.owl-carousel').append(some_var)
							});
						};
						var start_carousel = function(){
							var loop_carousel = target.find('.owl-carousel').children().size() > 1 ? true : false;
							target.find('.owl-carousel').owlCarousel({
								items: 1,
								loop: loop_carousel,
								nav: false,
								autoplay: true,
								pullDrag: false,
								center: true,
								navigation: true,
								navigationText : true,
								responsive:{
									0: {
										items: 1
									}
								}
							});
						};

						var init = function(){
							get_model_data(function(){
								build_dom();
								start_carousel();
							})
						};

						init();

					},
					renderHomepageBoxes: function(target){
						var that = this;
						var data = {};
						var views = {'homeBoxView': that.php_homepageBox_item};
						var getModel = function(callback){
							that.returnAndSaveJsonData('listHomeBoxes', function(listHomeBoxes){
								data.homeBoxData = listHomeBoxes;
								callback();
							});
						};
						var buildDOM = function(){
							var homeBoxData = data.homeBoxData;
							var homeBoxView = views.homeBoxView;
							$.each(homeBoxData, function(index_homeBoxData, value_homeBoxData){
								var returnObj = that.renderModel(value_homeBoxData, $(homeBoxView));
								target.append(returnObj);
							});
						};
						var init = function(){
							getModel(function(){
								buildDOM();
							});
						}
						init();
					},
					renderLinksListing: function(target){
						var that = this;
						var views = {
							'link_item': that.php_link_item
						};

						var collections = {};

						var get_model_data = function(callback){
							that.returnAndSaveJsonData('listLinks', function(linksData){
								collections['linksData'] = linksData;
								callback();
							})
						};

						var build_dom = function(){
							var linksData = collections.linksData;
							var linkItemView = views.link_item; 
							_.each(linksData, function(value_linksData, index_linksData){
								var thisLinkURL = that.externalURL(value_linksData.link_url);
								var returnObject = that.renderModel(value_linksData, $(linkItemView));
								returnObject.find('.the_title').attr('href', thisLinkURL);
								target.append(returnObject);
							})
						};

						var init = function(){
							get_model_data(function(){
								build_dom();
							})
						};

						init();
					},
					renderHomeCarousel: function(target){
						var that = this;
						var views = {
							'home_slide_item': that.php_home_slide_item
						}

						var get_model_data = function (callback) {
							that.returnAndSaveJsonData('listHomeSlides', function(){
								callback();
							});
						};

						var build_dom = function(){
							target.append('<div class="owl-carousel" />')
							var homeSlidesData = webApp.jsonDataCollection['listHomeSlides'];
							_.each(homeSlidesData, function(value_homeSlidesData, index_homeSlidesData){
								var returnObject = that.renderModel(value_homeSlidesData, $(views.home_slide_item))
								returnObject.data('homeSlidesData', value_homeSlidesData)
								target.find('.owl-carousel').append(returnObject)
							});
						};

						var initialize_carousel = function(){
							var carousel_loop = target.find('.owl-carousel').children().size() > 1 ? true : false;
							var carousel_nav = target.find('.owl-carousel').children().size() > 1 ? true : false;

							var owl = target.find('.owl-carousel');
							owl.on('initialized.owl.carousel', function(){
								target.find('.homeSlideItem').each(function(){
									if(typeof $(this).data('homeSlidesData') !== "undefined"){
										var this_slides_data = $(this).data('homeSlidesData');
										var this_featured_image = this_slides_data.featuredImage;
										$(this).backstretch(this_featured_image);
										$(this).find(".the_content").textFit({alignHoriz:true, alignVert:true})
									}
								})
							});
							owl.owlCarousel({
							    loop: false,
							    nav: false,
							    pullDrag: false,
							    responsive:{
							        0:{
							            items:1
							        }
							    }
							});
						};
						var init = function(){
							get_model_data(function(){
								build_dom();
								initialize_carousel();
							})
						};
						init();
					},
					render_featured_image: function(target){
						var that = this;
						var target = (typeof target !== "undefined") ? target : $('body');
						var collected_data = {}
						var thisPage = that.slugify(pageID);
						var buildDom = function(){
							var thisFeaturedImageData = collected_data['listFeaturedImages']
							var this_featured_image = thisFeaturedImageData.featuredImage;
							if(this_featured_image != null){
								target.backstretch(this_featured_image);
							}
						}
						var getData = function(callback){
							var thisPageData = that.accessor_listPages[thisPage];
							collected_data['listFeaturedImages'] = thisPageData;
							callback();
						}
						var init = function(){
							getData(function(){
								buildDom();
							});
						}
						init();
					},			
					bindMenuAnimations: function(menuTarget){
						menuTarget.children('li').mouseenter(function(){
							var hoverElement = $(this);
							var animationElement = hoverElement.children('ul');

							if(animationElement.size() > 0){

								dynamics.css(animationElement[0], {
									opacity: 0,
									display: 'block',
									scale: 1.1
								})
								dynamics.animate(animationElement[0], {
									scale: 1,
									opacity: 1
								}, {
									change: function(){
										if(typeof hoverElement.firstRun === "undefined" || hoverElement.firstRun == 0){
											hoverElement.firstRun = 1;
											hoverElement.siblings('li').children('ul').css('display','none');

											hoverElement.children('ul').children('li').each(function(index){
												var item = $(this)[0]
												dynamics.css(item, {
													opacity: 0,
													translateY: 20
												})

												dynamics.animate(item, {
													opacity: 1,
													translateY: 0
												}, {
													type: dynamics.spring,
													frequency: 300,
													friction: 435,
													duration: 1000,
													delay: 100 + index * 40
												})
											})

										}
									},
									complete: function(){
									},
									duration: 1000,
									type: dynamics.spring,
									frequency: 300,
									friction: 900,
								})
							}
						}).mouseleave(function(){
							var hoverElement = $(this);
							var animationElement = hoverElement.children('ul');

							if(animationElement.size() > 0){
								dynamics.animate(animationElement[0], {
									opacity: 0
								}, {
									complete: function(){
										animationElement.css('display', 'none');
									},
									delay: 300,
									duration: 300
								})
							}
						});

					},
					cleanUp: function(pageIDrequest, postIDrequest){
						var that = this;

						// if IE 8
						if(!($('html').hasClass('internet-explorer-8') || js_mobile == "true")){
							that.bindMenuAnimations($('#menu-main-menu'));
						}

						// if mobile device
						if(js_mobile == "true"){
							if($('#mm-navbar').size() == 0){
								that.multiLevelPushMenu(jQuery('#navbar'))
							}
							$("#mm-navbar").trigger("close.mm");
						}

						// page specific cleanups
						switch(pageIDrequest){
							default:
							break;
						}
					},
					adminPages: function(currentPage){
						var that = this;
						switch(currentPage){
							case "galleries":
								that.renderGalleriesAdmin();
							break;

							case "page":
							case "events":
								that.featuredImagesLauncher();
							break;
						}
					},
					singlePageAnimate: function(pageIDrequest){
						var that = this;
						that.animateSomething({
							target: $('body'),
							animation: {
								scrollTop: $('#page_' + pageIDrequest).offset().top - $('.pageSection').offset().top,
							},
							time: 400,
							callback: function() {
								console.log('page loaded')
							}
						})
				    }
				}
			});
		}

	});

})(jQuery);

