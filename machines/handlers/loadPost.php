<?php
	// ini_set('display_errors', 1);
	// error_reporting(E_ALL);

	require_once realpath(dirname(__FILE__).'/../../../../..').'/wp-load.php';

	$postIDarray = $_POST['postRequest'];

	$returnObject = array();
	foreach ($postIDarray as $key => $value) {
		$postData = get_post($value);

		$postMetaData = get_post_meta($value, '', true);

		$returnPostObject = array();
		$returnPostObject['image_data'] = buildAttachmentData($value);

		foreach ($postData as $key1 => $value1) {
			$returnPostObject[$key1] = $value1;
		}

		foreach ($postMetaData as $key1 => $value1) {
			$returnPostObject[$key1] = $value1[0];
		}

		$returnObject[$value] = $returnPostObject;
	}

	echo json_encode($returnObject);

?>