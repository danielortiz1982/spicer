<?php
	require_once realpath(dirname(__FILE__).'/../../../../..').'/wp-load.php';

	class buildWordpressMenu {
		public function __construct($siteMapRequest) {
			$this->printSiteMap($siteMapRequest);
			$this->renderMenu($siteMapRequest);
			// $this->debug();
		}

		private function printSiteMap($siteMap){
			echo "<pre>";
			print_r($siteMap);
			echo "</pre>";
		}

		private function renderMenu($multiDimensionalArray, $recursionTracker = 0, $parentKey = 0){
			$recursionTracker++;
			foreach ($multiDimensionalArray as $key => $value) {
				$keyParent = $this->addPageMenu($key, $parentKey);
				if(is_array($value)){
					$this->renderMenu($value, $recursionTracker, $keyParent);
				}
			}
		}

		private function addPageMenu($pageName, $parentMenuItemID){
			if( is_null(get_page_by_title($pageName)) ){
				$my_post = array(
					'post_title'    => $pageName,
					'post_type'  => 'page',
					'post_status'   => 'publish',
					'post_author'   => 1,
					'post_content' => file_get_contents('http://loripsum.net/api')
				);
				wp_insert_post($my_post);

				$mymenu = wp_get_nav_menu_object('Main Menu');
				$menuID = (int)$mymenu->term_id;
				$myPage = get_page_by_title($pageName);

				$itemData =  array(
				    'menu-item-object-id' => $myPage->ID,
				    'menu-item-parent-id' => $parentMenuItemID,
				    'menu-item-object' => 'page',
				    'menu-item-type' => 'post_type',
				    'menu-item-status' => 'publish'
				);
				$keyParent = wp_update_nav_menu_item($menuID, 0, $itemData);
				return $keyParent;
			} else {
				echo "page already exists <br />";
			}
		}
	}

	$obj = new buildWordpressMenu(array(
		'Home' => null,
		'About' => null,
		'Services' => null,
		'Contact' => null,
	));

?>