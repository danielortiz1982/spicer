(function($){

	boilerplate = function(variableName){
		this.ajaxer = (typeof ajaxer === "undefined") ? false : ajaxer;
		if($('html').hasClass('internet-explorer-8')){
			this.ie8support();
		}
	};

	boilerplate.prototype.multiLevelPushMenu = function(target, position) {

		position = (typeof position === "undefined") ? 'right' : position;

		targetClone = target;
		targetClone.find('*').removeAttr('class')
		targetClone.removeAttr('class')

		targetClone.mmenu({
			autoHeight: true,
			offCanvas: {
				position: position
			},
			onClick: {
				close: function() {
					return ($(this).attr('data-pageid') != "");
				}
			}
		}, {
			clone: true
		});

		$('.mm-page').append($('.pageSection'))
		$('.mm-page').append($('.footer'))

		$('.navbar-toggle').on('click', function(e){
			if($('#mm-navbar').hasClass('mm-opened')){
				$("#mm-navbar").trigger("close.mm");
			} else {
				$("#mm-navbar").trigger("open.mm");
			}
		});

	}

	boilerplate.prototype.loadView = function(pageIDrequest, postIDrequest){
		var that = this;
		that.cleanUp(pageIDrequest, postIDrequest);

		if( that.isSinglePage() ){
			that.returnAndSaveJsonData('listPages', function(pagesData){
				var pageListing = [that.defaultPage];
				$('#navbar').find('#menu-main-menu').find('a').each(function(){
					if($.inArray($(this).attr('data-pageid'), pageListing) == -1){
						pageListing.push($(this).attr('data-pageid'))
					}
				})


				_.each(pageListing, function(value_pageListing, index_pageListing){
					var viewName = (typeof that.viewRender[value_pageListing] === 'undefined') ? 'default' : value_pageListing;
					that.viewRender[viewName](that, value_pageListing);
				})

				setTimeout(function(){
					that.singlePageAnimate(pageIDrequest)
				}, 400)
			})
		} else {

			pageAttrID = ($.isNumeric(pageID.charAt(0))) ? "_" + pageID : pageID;
			$('body').data('pageid', pageID);
			$('body').attr('id', pageAttrID);

			var viewName = (typeof that.viewRender[pageIDrequest] === 'undefined') ? 'default' : pageIDrequest;
			that.viewRender[viewName](that);

		}

	}


	boilerplate.prototype.executeLogin = function(redirectPage){
		var that = this;
		Shadowbox.init();
		Shadowbox.open({
			// player: 'iframe',
			// content: 'http://www.ifmacap.org/preview/wp-content/themes/ccifma/machines/handlers/linkedin.api.php'
			content: '<div class="popUpLoginForm" />',
			player: 'html',
			options: {
				onFinish: function() {
					formObject = $(that.php_ajax_login);
					$('.popUpLoginForm').html(formObject)

					thisForm = $('.popUpLoginForm form');

					// handle submit
					thisForm.on('submit', function(e) {
						e.preventDefault();

						loginInfoArr = {
							'username': $(this).find('#user_login').val(),
							'password': $(this).find('#user_pass').val()
						}

						$.post(that.pageDir + "/machines/handlers/ajaxLogin.php", loginInfoArr, function(data) {
							if (data == 'success') {
								Shadowbox.close();
								js_user_logged_in = true;
								window.location.href = that.pathPrefix + redirectPage;
							} else {
								if (thisForm.find('.wrongLogin').size() <= 0 ) {
									thisForm.append('<p class="wrongLogin">Wrong username or password</p>');
								}
							}
						});

					});
				}
			}
		});
	}

	boilerplate.prototype.externalURL = function(urlRequest){
		return (urlRequest.indexOf("http:") == -1 && urlRequest.indexOf("https:") == -1) ? "http://" + urlRequest : urlRequest;
	}

	boilerplate.prototype.renderModel = function(data, viewObject){

		_.each(data, function(value, index){
			switch (index){
				default:
					viewObject.find('.' + index).html(_.unescape(value));
				break;
			}
		});

		return viewObject;
	}

	boilerplate.prototype.returnAndSaveJsonData = function(jsonRequest, callback){
		var that = this;
    	if(typeof that.jsonDataCollection === "undefined"){
    		that.jsonDataCollection = {};
    	}
        args = {};
        args['idArray'] = null;
        if(typeof callback !== "undefined"){
	        if(typeof that.jsonDataCollection[jsonRequest] === "undefined"){
		        returnedJsonData = $.post(that.pageDir + "/machines/handlers/loadJSON.php", { jsonRequest: jsonRequest, args: args }, function(data) {
		        	that.jsonDataCollection[jsonRequest] = data;
					that.createAccessor(data, jsonRequest);
		        	callback(data);
		        }, 'json');
		    } else {
				that.createAccessor(that.jsonDataCollection[jsonRequest], jsonRequest);
		    	callback(that.jsonDataCollection[jsonRequest])
		    }
        } else {
	        if(typeof that.jsonDataCollection[jsonRequest] === "undefined"){
		        returnedJsonData = $.post(that.pageDir + "/machines/handlers/loadJSON.php", { jsonRequest: jsonRequest, args: args }, function(data) {
		        	that.jsonDataCollection[jsonRequest] = data;
					that.createAccessor(data, jsonRequest);
		        	return data;
		        }, 'json');
		    } else {
				that.createAccessor(that.jsonDataCollection[jsonRequest], jsonRequest);
		    	return that.jsonDataCollection[jsonRequest];
		    }
        }
    }

	boilerplate.prototype.createAccessor = function(dataSource, variableName){
		var that = this;
		if(typeof that['accessor_' + variableName] === "undefined"){
			that['accessor_' + variableName] = {};
			if(variableName == 'listPages'){
				_.each(dataSource, function(valuePagesData, indexPagesData){
					that['accessor_' + variableName][that.slugify(valuePagesData.the_title)] = valuePagesData
				});
			} else {
				_.each(dataSource, function(valuePagesData, indexPagesData){
					that['accessor_' + variableName][valuePagesData.post_id] = valuePagesData
				});
			}
		}
	}

    boilerplate.prototype.startUp = function(){
    	var that = this;
    	that.setPageID(that.RewriteBase);

		if(window['pageID'] == "admin"){
			that.loadDatePicker($('.date'));
			that.loadSortable($(".sortable"));
			if($('#people_sample_hidden_meta').size() != 0){
				$('#people_sample_hidden_meta').find('.hidden_meta').val('I was generated dynamically')
			}
			$('head').append('<link rel="stylesheet" id="jquery-style-css" href="' + that.pageDir + '/styles/admin-styles.min.css" type="text/css" media="all">');
			that.adminPages(pagenow);
		} else {
			that.setPopstate();
			that.fixLinks();

			if(typeof pageID !== "undefined"){
				that.loadView(pageID, postID);
				that.startUpRan = true;
			}
		}
    };

	boilerplate.prototype.fixLinks = function(params){
		var that = this;
		$('.navbar, .footer').find('a').each(function(){
			var isInternalLink = ($(this).hasClass('hyperlink')) ? false : true;
			var isNotEmail =  ($(this).attr('href').indexOf('mailto:') == -1) ? true : false;
			var isLogo = ($(this).hasClass('navbar-brand')) ? true : false;

			if(isInternalLink && isNotEmail){
				var pageSlug = (isLogo) ? that.defaultPage : that.slugify($(this).text().replace('&amp;', ''))
				var newURL = '/' + pageSlug + '/';
				$(this).attr('href', newURL);
				$(this).addClass('menu-item-' + pageSlug);
				$(this).attr('data-pageid', pageSlug);

				$(this).off('click');
				$(this).on('click', function(e){
					e.preventDefault();
					that.pushPageNav($(this).attr('data-pageid'))
				})
			}
		});
	}

    boilerplate.prototype.animateSomething = function(params){
    	$(params.target).animate(params.animation, params.time, params.callback)
    }

    boilerplate.prototype.isSinglePage = function(){
    	var that = this;
    	return (typeof this.singlePage !== "undefined" && this.singlePage != null && this.singlePage != false);
    }

	boilerplate.prototype.pushPageNav = function(pageIDrequest, postIDrequest){
		var that = this;
    	if(typeof postIDrequest === "undefined"){
    		postIDrequest = "";
    		postIDurl = "";
    	} else {
    		postIDurl = postIDrequest + "/";
    	}



		if( that.isSinglePage() ){

			if((window.location.pathname.charAt(window.location.pathname.length-1) == "/") && (window.location.pathname != that.RewriteBase)){
				newPage = '/' + pageIDrequest + "/" + postIDurl;
			} else {
				newPage = '/' + pageIDrequest + "/" + postIDurl;
			}
			var stateObj = { pageID: newPage};
	        if (Modernizr.history){
				history.pushState(stateObj, null, newPage);
	        }
			that.setPageID(that.RewriteBase)

			that.singlePageAnimate(pageIDrequest)

		} else {
	        if (Modernizr.history){
				that.animateSomething({
					target: $('.pageSection'),
					animation: {
						opacity: 0,
					},
					time: 200,
					callback: function() {
						if((window.location.pathname.charAt(window.location.pathname.length-1) == "/") && (window.location.pathname != that.RewriteBase)){
							newPage = '/' + pageIDrequest + "/" + postIDurl;
						} else {
							newPage = '/' + pageIDrequest + "/" + postIDurl;
						}
						var stateObj = { pageID: newPage};
						history.pushState(stateObj, null, newPage);
						that.setPageID(that.RewriteBase)
						that.gaSendPageView();
						that.loadView(pageID, postID);
					}
				})
	        } else {
				window.location = that.pathPrefix + pageIDrequest + "/" + postIDurl;
	        }
		}

    }

    boilerplate.prototype.setPopstate = function(){
    	var that = this;
		if( that.isSinglePage() ){

		    $(window).on('popstate',function(){
				if(that.startUpRan){
					postID = "";
					previousPage = pageID;


			    	if(history.state != null){
						that.setPageID(that.RewriteBase, history.state.pageID);
						that.singlePageAnimate(pageID)

			    	} else {
		    			loadPage = that.cleanPageState(window.location.pathname.replace(that.RewriteBase, ""));

		    			postIDtest = window.location.pathname.split("/");
		    			if(postIDtest[postIDtest.length-1] == ""){
		    				postIDtest.pop();
		    			}
		    			if($.isNumeric(postIDtest[postIDtest.length-1])){
		    				postID = postIDtest[postIDtest.length-1];
		    			}

		    			if(loadPage == ""){
		    				loadPage = that.defaultPage;
		    			}
		    			pageID = loadPage;

		    			postIDpath = (history.state == null) ? "" : history.state.pageID;
						that.setPageID(that.RewriteBase, postIDpath);
						that.singlePageAnimate(pageID)

			    	}
			    }
		    });


		} else {
		    $(window).on('popstate',function(){
				if(that.startUpRan){
					postID = "";
					previousPage = pageID;


			    	if(history.state != null){
						that.animateSomething({
							target: $('.pageSection'),
							animation: {
								opacity: 0,
							},
							time: 200,
							callback: function(){
								that.setPageID(that.RewriteBase, history.state.pageID);
								that.gaSendPageView();
					    		that.loadView(pageID, postID);
							}
						})
			    	} else {
		    			loadPage = that.cleanPageState(window.location.pathname.replace(that.RewriteBase, ""));

		    			postIDtest = window.location.pathname.split("/");
		    			if(postIDtest[postIDtest.length-1] == ""){
		    				postIDtest.pop();
		    			}
		    			if($.isNumeric(postIDtest[postIDtest.length-1])){
		    				postID = postIDtest[postIDtest.length-1];
		    			}

		    			if(loadPage == ""){
		    				loadPage = that.defaultPage;
		    			}
		    			pageID = loadPage;

						that.animateSomething({
							target: $('.pageSection'),
							animation: {
								opacity: 0,
							},
							time: 200,
							callback: function(){
								var pastState;
								if(history.state != null){
									pastState = history.state.pageID;
								} else {
									pastState = "";
								}
								that.setPageID(that.RewriteBase, pastState);
								that.gaSendPageView();
					    		that.loadView(pageID, postID);
							}
						})

			    	}
			    }
		    });
		}
    }

	boilerplate.prototype.changePage = function(transition){
		var that = this;
		that.animateSomething({
			target: $('.pageSection'),
			animation: {
				opacity: 1,
			},
			time: 200,
			callback: function(){
				that.animateSomething({
					target: $('html,body'),
					animation: {
						scrollTop: 0 
					},
					time: 100,
					callback: function(){
					}
				})
			}
		})
	}

	boilerplate.prototype.cleanPageState = function(historyState){
		postIDtest = historyState.split("/");
		if(postIDtest[postIDtest.length-1] == ""){
			postIDtest.pop();
		}
		if($.isNumeric(postIDtest[postIDtest.length-1])){
			postIDtest.pop();
			historyState = postIDtest.join("/");
		} else {
			historyState = postIDtest[0];
		}
		if(typeof historyState !== "undefined"){
			historyState = historyState.replace("../","");
			historyState = historyState.replace("/","");
			historyState = historyState.replace(/\.\.+/g, '');
			historyState = historyState.replace(/\/+/g, '');
		} else {
			historyState = "";
		}
		return historyState;
	}

	boilerplate.prototype.slugify = function(text){

		slugText = text.replace(/&amp;/g, '')
		    .replace(/#8216;/g, '')
		    .replace(/#038;/g, '')
		    .replace(/#8217;/g, '');


		slugText = slugText.toString().toLowerCase()
			.replace(/\+/g, '')           // Replace spaces with 
			.replace(/\s+/g, '-')           // Replace spaces with -
			.replace(/[^\w\-]+/g, '')       // Remove all non-word chars
			.replace(/\-\-+/g, '-')         // Replace multiple - with single -
			.replace(/^-+/, '')             // Trim - from start of text
			.replace(/-+$/, '');            // Trim - from end of text

		return slugText;

	}

	boilerplate.prototype.getUrlVars = function(params){
	    var vars = {};
	    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	        vars[key] = value;
	    });
	    return vars;
	};

	boilerplate.prototype.start = function(params){
		this.configDevMode();
		this.configParams(params);

		this.startUpRan = false;
		this.pathPrefix = window.location.protocol + "//" + window.location.host + "/";
		this.defaultPageDir = window.location.protocol + "//" + window.location.host + "/wp-content/themes/" + this.themeFolderName;
		this.pageDir = (typeof $('body').attr('data-tempdir') === "undefined") ? this.defaultPageDir : $('body').attr('data-tempdir');
		this.urlQueryString = "?" + Math.floor((Math.random()*10000)+1);

		if(typeof this.typekitID !== "undefined" && this.typekitID != null){
			this.loadTypekit(this.typekitID);
		} else {
			this.loadFilesToVariables();
		}
	};

	boilerplate.prototype.loadFilesToVariables = function() {
		var that = this;
		var fileArray = that.filesToVariablesArray;

        var ajaxRequests = [];
        _.each(fileArray, function(value_fileArray, index_fileArray){
        	var keyName = Object.keys(fileArray[index_fileArray]);
        	var variableURL = fileArray[index_fileArray][keyName];
        	var fileType = variableURL.split(".").slice(-1)[0];
        	var variableName = fileType + "_" + keyName;

	        ajaxRequest = $.ajax({
	            type: 'GET',
	            dataType: "text",
	            url: that.defaultPageDir + '/' + variableURL,
	            variableName: variableName,
	            variableURL: variableURL,
	            cache: false,
	            success: function (data, textStatus, jqXHR) {
	            	that[this.variableName] = data;
	            },
	            error: function (argument) {
	            }
	        });
	        ajaxRequests.push(ajaxRequest);
        });

        $.when.apply($, ajaxRequests).then(function() {
        	that.startUp()
        }, function() {
        	// errors occured
        });
    };

	boilerplate.prototype.loadSortable = function(target){
		target.sortable();
		target.disableSelection();
		target.on( "sortstop", function( event, ui ) {
			sortData = {};
			$(this).children('li').each(function(){
				sortData[$(this).data('id')] = $(this).index();
			});
			var data = {
				action: 'update_sort',
				sort_data: sortData
			};
			$.post(ajaxurl, data, function(response) {
			});					
		});
	}

	boilerplate.prototype.loadDatePicker = function(target, changeCallback){
		hiddenDate = target.siblings('input');
		target.datetimepicker();
		if(hiddenDate.val() == ""){
			var myDate = new Date();
			var prettyDate =(myDate.getMonth()+1) + '/' + myDate.getDate() + '/' + myDate.getFullYear() + " " + myDate.getHours() + ":" + myDate.getMinutes();
			target.val(prettyDate);
			hiddenDate.val(Date.parse(prettyDate)/1000);
		}
		target.change(function() {
			$(this).siblings('input').val(Date.parse($(this).val())/1000);
			dateArray = [{event_start: $('input[name="event_start"]').val(), event_end: $('input[name="event_end"]').val()}];
			$('#event_date_array_meta').find('.hidden_meta').val(JSON.stringify(dateArray));
			if(changeCallback){
				updateRepeatConfig();
			}
		});
	}

	boilerplate.prototype.renderGalleriesAdmin = function(){
		var that = this;
		$.post( that.pageDir + "/machines/handlers/getGalleries.php", {  }, function( data ) {
			// get all images from database and save to global variable
			window['galleryImagesObject'] = data;

			// append images to all images box
			_.each(data, function(value, index){
				returnObject = buildThumbnails(value, index);
				$('#galleries_all_images_meta').find('.inside').append(returnObject);
			});

			// css cleanup for side images box
			$('#galleries_all_images_meta').find('img').on('load', function(){
				$('#galleries_gallery_images_meta').css('min-height', $( "#galleries_all_images_meta" ).height())
				$('#galleries_gallery_images_meta').find('.inside').css('min-height', Math.round($( "#galleries_all_images_meta" ).height()))
			});

			// load jQuery UI draggable
			$('#galleries_all_images_meta').find('.inside').children().draggable({
				revert: true
			});

			$( "#galleries_gallery_images_meta" ).find('.inside').droppable({
				hoverClass: "greyBackground",
				drop: function( event, ui ) {
					// load jQuery UI draggable
					if(ui.helper.parents('.postbox').attr('id') == "galleries_all_images_meta"){
						thisObject = $(ui.helper.html());
						returnObject = buildThumbnails(galleryImagesObject[thisObject.attr('data-postid')], thisObject.attr('data-postid'), true);
						$( "#galleries_gallery_images_meta" ).find('.inside').append(returnObject);
						$( "#galleries_gallery_images_meta" ).find('.inside').addClass('added')
						$( "#galleries_gallery_images_meta" ).find('.inside').addClass('transitionClass')
						saveGallery();
						updateGalleryEvents();
					}
				}
			});

			if($('#galleries_gallery_image_array').val() != ""){
				// check if this is a saved post and update the sidebar accordingly
				currentImageArray = $('#galleries_gallery_image_array').val().split(",");
				_.each(currentImageArray, function(value, index) {
					_.each(galleryImagesObject, function(value1, index1){
						if(value == index1){
							returnObject = buildThumbnails(value1, index1, true);
							$( "#galleries_gallery_images_meta" ).find('.inside').append(returnObject);
						}
					})
				})
				updateGalleryEvents();
			}
		}, "json");

		function buildThumbnails(imageData, imageID, sidebarThumb){
			returnObject = $('<div class="imageContainer" />')
			imageBlock = $('<div class="imageBlock" data-postid="' + imageID + '" />');
			imageThumb = $('<img />');
			imageThumb.attr('src', imageData.thumb);
			imageBlock.append(imageThumb);
			returnObject.append(imageBlock)
			if(sidebarThumb){
				boxControls = $('<div class="boxControls" />');
				deleteButton = $('<div class="deleteButton">remove</div>')
				boxControls.append(deleteButton)
				returnObject.append(boxControls)
				returnObject.append(imageBlock)
			}
			return returnObject;
		}

		function saveGallery(){
			outputArray = [];
			$('#galleries_gallery_images_meta').find('.imageBlock').each(function(){
				outputArray.push($(this).attr('data-postid'))
			})
			$('#galleries_gallery_image_array').val(outputArray)
		}

		function updateGalleryEvents(){
			setTimeout(function(){
				$( "#galleries_gallery_images_meta" ).find('.inside').removeClass('added')
			}, 400);

			setTimeout(function(){
				$( "#galleries_gallery_images_meta" ).find('.inside').removeClass('transitionClass')
			}, 500);

			$('#galleries_gallery_images_meta').find('.ui-droppable').sortable({
			});

			$('#galleries_gallery_images_meta').off( "sortstop")
			$('#galleries_gallery_images_meta').on( "sortstop", function( event, ui ) {
				saveGallery();
			});

			$('.deleteButton').off('click')
			$('.deleteButton').on('click', function(){
				$(this).parents('.imageContainer').remove()
				saveGallery();
			})
		}
	}

	boilerplate.prototype.featuredImagesLauncher = function(){
		var that = this;
		Shadowbox.init();
		$('.featured_images_launcher').on('click', function(){
			Shadowbox.open({
				content:    '<div class="featuredImages"><div class="frame"></div><div class="frame"></div></div>',
				player:     "html",
				title:      "Welcome",
				height:     100000000,
				width:      100000000,
				options: {onFinish: function () {
					frame1 = $('.featuredImages').find('.frame').eq(0);
					frame2 = $('.featuredImages').find('.frame').eq(1);

					frame1.addClass('imagePicker')
					frame2.append('<div class="imageOrderer" />')

					$.post( that.pageDir + "/machines/handlers/getGalleries.php", {  }, function( data ) {
						// get all images from database and save to global variable
						window['galleryImagesObject'] = data;

						_.each(data, function(image_value, image_index){
							returnObject = $('<div class="imageThumb" />');
							imageObject = $('<img />');
							imageObject.attr('src', image_value.thumb)
							returnObject.append(imageObject)
							returnObject.attr('data-index', image_index)
							returnObject.data('imageIndex', '.imageIndex' + image_index)
							returnObject.addClass('imageIndex' + image_index)
							frame1.append(returnObject)
						});

						$('.imageOrderer').sortable();
						$('.imageOrderer').on( "sortstop", function( event, ui ) {
							outputArray = [];
							$('.imageOrderer').find('.imageThumb').each(function(){
								outputArray.push($(this).attr('data-index'))
							})
							$('.featured_images_meta').val(outputArray)
						});

						if($('.featured_images_meta').val() != ""){
							selectedImages = $('.featured_images_meta').val().split(",")
							$.each(selectedImages, function(index, value){
								$('.imagePicker').find('.imageIndex' + value).addClass('selected')
								frame2.find('.imageOrderer').append($('.imagePicker').find('.imageIndex' + value).clone())
							});
						}

						frame1.find('.imageThumb').on('click', function(){
							if($(this).hasClass('selected')){
								$(this).removeClass('selected')
								targetClass = $(this).data('imageIndex')
								frame2.find(targetClass).remove()
							} else {
								$(this).addClass('selected')
								duplicateImage = $(this).clone()
								duplicateImage.removeClass('selected')
								frame2.find('.imageOrderer').append(duplicateImage)
							}
							outputArray = [];
							$('.imageOrderer').find('.imageThumb').each(function(){
								outputArray.push($(this).attr('data-index'))
							})
							$('.featured_images_meta').val(outputArray)
						});

					}, "json");								

				}} 

			});
		})
	}

	boilerplate.prototype.setPageID = function(pagePath, postIDpath){
		var that = this;
		if($('body').hasClass("wp-admin")){
			window['pageID'] = "admin";
		} else {
			var pageIDFound = false;
			var urlArray = window.location.pathname.replace(pagePath, '');
				urlArray = urlArray.split("/");

			if(urlArray[urlArray.length-1] == ""){
				urlArray.pop();
			}

			if(typeof urlArray[0] === "undefined"){
				pageIDFound = true;
				window['pageID'] = this.defaultPage;
			} else {
				_.each(json_pages, function(value, index){
					if(value.pageID == urlArray[0]){
						pageIDFound = true;
						window['pageID'] = value.pageID;
					}
				});
			}

			if(!pageIDFound){
				if(!that.ajaxer){
					that.execute404();
				}
			} else {
			// second array item must be itemID
				if(urlArray.length == 2){
					switch(urlArray[0]){
						default:
							postIDFound = true;
							window['postID'] = urlArray[1];
						break;
					}
				} else {
					window['postID'] = "";
				}

			}
		}
	};

	boilerplate.prototype.execute404 = function(){
		$('section').html("<h1>This page was not found, you are being redirect to the home page</h1>");
		window.location = this.pathPrefix + this.defaultPage;
	}

	boilerplate.prototype.configParams = function(paramsRequest){
		var that = this;
		_.each(paramsRequest, function(value_params, index_params){
			switch(index_params){
				case 'themeFolderName':
					var urlVars = that.getUrlVars();
					if(typeof urlVars.dev !== "undefined" && urlVars.dev != "" ){
						themeAppendString = urlVars.dev;
					} else {
						themeAppendString = "dev";
					}
					that[index_params] = (typeof js_dev_mode === "undefined" || js_dev_mode == "false") ? value_params : value_params + "-" + themeAppendString;
				break;

				default:
					that[index_params] = value_params;
				break;
			}
		});

		_.each(that.helpers, function(value_helper, index_helper){
			boilerplate.prototype[index_helper] = value_helper;
		});

	};

	boilerplate.prototype.configDevMode = function(){
		if(typeof js_dev_mode !== "undefined" && js_dev_mode == "true" && Modernizr.history && (js_dev_refresh == "true")){
			history.pushState(null, null, window.location.origin + window.location.pathname);
		}
	};

	boilerplate.prototype.loadTypekit = function(typekitID){
		var that = this;
		var config = {
			kitId: typekitID,
			scriptTimeout: 1000,
			loading: function() {
				// JavaScript to execute when fonts start loading
			},
			active: function() {
				that.loadFilesToVariables();
			},
			inactive: function() {
				that.loadFilesToVariables();
			}
		};
		var h=document.getElementsByTagName("html")[0];h.className+=" wf-loading";var t=setTimeout(function(){h.className=h.className.replace(/(\s|^)wf-loading(\s|$)/g," ");h.className+=" wf-inactive"},config.scriptTimeout);var tk=document.createElement("script"),d=false;tk.src='//use.typekit.net/'+config.kitId+'.js';tk.type="text/javascript";tk.async="true";tk.onload=tk.onreadystatechange=function(){var a=this.readyState;if(d||a&&a!="complete"&&a!="loaded")return;d=true;clearTimeout(t);try{Typekit.load(config)}catch(b){}};var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(tk,s)
	};

	boilerplate.prototype.saveAjax = function() {
		var that = this;
		var newDOM = $('html').clone();

		$.post( that.pageDir + "/machines/handlers/saveAjax.php", { pageID: pageID, postID: postID, html: newDOM[0].outerHTML })
		.done(function(data) {
			console.log(data)
		});
	}

	// pageID should be undefiend for listPages
	// loop will take 5 seconds between pages
	// 2 second delay to execute saveAhax
	boilerplate.prototype.saveAjaxArray = function(modelList, pageID) {
		var that = this;
		that.returnAndSaveJsonData(modelList, function(modelData){
			var index = 0;
			window['switchLoop'] = setInterval(function(){
				if(typeof pageID == 'undefined'){
					var postID = that.slugify(modelData[index].the_title);
					that.pushPageNav(postID);
				} else {
					var postID = that.slugify(modelData[index].the_title) + '-' + modelData[index].post_id;
					that.pushPageNav(pageID,  postID);
				}

				setTimeout(function(){
					that.saveAjax()
					console.log(index, modelData.length-1);

					if( index == modelData.length-1 ) {
						clearInterval(switchLoop)
					} else {
						index++
					}
				}, 2000)
			}, 5000);
		});
	};

	/**
	 * [xmlSitemap generate site]
	 * @param  {array} models array of objects
	 * 
	 * exp: [{
	 * 	
	 * 	LIST MODEL 
	 * 	modelName:'listProjects', 
	 * 	
	 * 	MODEL PAGE ID LEAVE BLANK IF NONE
	 * 	modelUrl: '/portfolio/', 
	 * 	
	 * 	ARRAY FORMING SINGLE PAGE URL NAME
	 * 	THE SAMPEL BELOW WILL GENERATE
	 * 	sample_page-1/
	 * 	pageName: ['{the_title}', '-' '{post_id}']
	 * 	}]
	 */
	
	 /* 
	 EXAMPLE:
	 webApp.xmlSitemap([{ 
	 	modelName:'listPages', 
	 	modelUrl: '/page',
	 	pageName: ['the_title', '-', 'post_id']
	 }, { 
	 	modelName:'listPeople', 
	 	modelUrl: '/news',
	 	pageName: ['the_title']
	 }])
*/
	boilerplate.prototype.saveXMLSitemap = function(models) {
		var that = this;
		var sitemapArray = [
			'<?xml version="1.0" encoding="UTF-8"?>',
			'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">'
		]

		buildXML = function(index){
			var modelData = models[index];
			var nextIndex = (index+1);

			var listModel = modelData.modelName;
			var modelPath = location.origin;
			var pageName = modelData.pageName

			modelData.modelUrl = ( _.isUndefined(modelData.modelUrl) )? '/' : modelData.modelUrl;
			// check modeUrl starts with /
			modelData.modelUrl = ( modelData.modelUrl.charAt(0) == '/' )? modelData.modelUrl : '/' + modelData.modelUrl;
			// check modeUrl ends with /
			modelData.modelUrl = ( modelData.modelUrl.charAt(modelData.modelUrl.length-1) == '/' )? modelData.modelUrl : modelData.modelUrl + '/';
			
			modelPath += modelData.modelUrl;

			that.returnAndSaveJsonData(listModel, function(singleModelData){
				_.each(singleModelData, function(singleItemValue, singleItemIndex){
					var pageSlug = '';
					
					_.each(pageName, function(nameParam, nameIndex){
						if( _.isUndefined(singleItemValue[nameParam]) ){
							pageSlug += nameParam
						} else {
							pageSlug += that.slugify( String(singleItemValue[nameParam]) );
						}
					});

					var pageObj = $('<url/>');
					var locObj = $('<loc/>', {
						html: modelPath + pageSlug + '/'
					});
					var freqObj = $('<changefreq/>', {
						html: 'daily'
					});

					pageObj.append('\n', locObj, '\n', freqObj, '\n');
					sitemapArray.push(pageObj[0].outerHTML)
				});

				console.log(models.length, nextIndex, models.length == nextIndex)
				if( models.length == nextIndex ){
					saveData();
				} else {
					buildXML(nextIndex);
				}
			});
		}

		saveData = function(){
			sitemapArray.push('</urlset>');
			
			$.post(that.pageDir + "/machines/handlers/saveSitemap.php", {
				data: sitemapArray.join('\n')
			}, function(response){
				console.log(response)
			});
		}

		buildXML(0);
	};

	boilerplate.prototype.ie8support = function(){
		// object.keys support
		    if (!Object.keys) {
		        Object.keys = (function () {
		            'use strict';
		            var hasOwnProperty = Object.prototype.hasOwnProperty,
		            hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
		            dontEnums = [
		            'toString',
		            'toLocaleString',
		            'valueOf',
		            'hasOwnProperty',
		            'isPrototypeOf',
		            'propertyIsEnumerable',
		            'constructor'
		            ],
		            dontEnumsLength = dontEnums.length;
		            return function (obj) {
		                if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
		                    throw new TypeError('Object.keys called on non-object');
		                }
		                var result = [], prop, i;
		                for (prop in obj) {
		                    if (hasOwnProperty.call(obj, prop)) {
		                        result.push(prop);
		                    }
		                }
		                if (hasDontEnumBug) {
		                    for (i = 0; i < dontEnumsLength; i++) {
		                        if (hasOwnProperty.call(obj, dontEnums[i])) {
		                            result.push(dontEnums[i]);
		                        }
		                    }
		                }
		                return result;
		            };
		        }());
		    }

		// console support
	    var method;
	    var noop = function () {};
	    var methods = [
	        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
	        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
	        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
	        'timeStamp', 'trace', 'warn'
	    ];
	    var length = methods.length;
	    var console = (window.console = window.console || {});
	    while (length--) {
	        method = methods[length];
	        if (!console[method]) {
	            console[method] = noop;
	        }
	    }
	};

	    boilerplate.prototype.gaSendPageView = function () {
    	var url_string = (typeof postID === "undefined" || postID == null || postID == "") ? '/' + pageID + '/' : '/' + pageID + '/' + postID + '/';
    	ga('send', 'pageview', url_string);
    }

	boilerplate.prototype.simpleSearch = function(searchTermArray, dataSource, inclusionArray) {
		
		inclusionArray = (typeof inclusionArray === "undefined") ? "" : inclusionArray;
		exclusionArray = ['attachments', 'post_id', 'featuredImage', 'partner_industry', 'partner_level'];
		resultArray = [];
		deleteArray = [];
		_.each(searchTermArray, function(value, index){
			searchTermArray[index] = value.replace(/\W/g, '');			
			if(searchTermArray[index] == ""){
				deleteArray.push(index);
			}
		});
		deleteArray = deleteArray.reverse();
		_.each(deleteArray, function(value, index){
			searchTermArray.splice(value, 1);
		})
		_.each(searchTermArray, function(value0, index0){
			_.each(Object.keys(dataSource[0]), function(value, index){

				if((inclusionArray == "") || (inclusionArray.indexOf(value) != -1)){

					if(exclusionArray.indexOf(value) == -1){
						results = _.filter(dataSource, function(results, second, third){
							if(typeof results[value] === "string"){
								return (results[value].toLowerCase().indexOf(value0) >= 0);
							}
						});
						resultArray.push(results);
					}
				}
			});
		});
		resultArray = _.union(resultArray);
		resultArray = _.flatten(resultArray);
		resultArray = _.union(resultArray);
		return resultArray;

	}

	boilerplate.prototype.dollarize = function(dollars){
		dollars = parseFloat(dollars).toFixed(0);
		dollars = "$" + String(dollars).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
		return dollars;
	}

	boilerplate.prototype.postIDnotSet = function(){
		return (typeof postID === "undefined") || (postID == "")
	}

	boilerplate.prototype.renderBlog = function(){
		var that = this;
		var blogView = $(that.php_blog_item);
		var commentView = blogView.find('.commentItem').eq(0).clone();
		blogView.find('.commentItem').remove();

		var loadComments = function(itemID){
			$.post(that.pageDir + "/machines/handlers/loadComments.php", {'id': itemID}, function(data) {
				var thisBlogItem = $('.blogItem' + itemID);
				thisBlogItem.find('.commentList').html('')
				_.each(data, function(value_data, index_data){
					var thisComment = that.renderModel(value_data, commentView.clone());
					thisBlogItem.find('.commentList').append(thisComment);
				});
			}, "json");
		};

		var renderBlogItem = function(){
			var numbericPostID = postID.split('-');
			numbericPostID = parseInt(numbericPostID[numbericPostID.length - 1]);

			that.returnAndSaveJsonData('listBlogItems', function(blogData){
				var thisBlogItem = that.accessor_listBlogItems[numbericPostID];
				var thisBlogView = that.renderModel(thisBlogItem, blogView.clone());
				thisBlogView.find('.commentForm').data('blogid', thisBlogItem.post_id);
				thisBlogView.addClass('blogItem' + thisBlogItem.post_id);

				thisBlogView.css({
					'border-top': '12px solid black',
					'margin-top': '12px',
					'padding-top': '12px'
				});

				$('.pageSection').html('<div class="container" />');
				$('.pageSection').find('.container').html(thisBlogView);
				
				processCommentForm(thisBlogView.find('.commentForm'), false, loadComments);

				loadComments(thisBlogItem.post_id);
				that.changePage();
			});

		};

		var renderBlogListing = function(){
			that.returnAndSaveJsonData('listPages', function(pagesData){
				thisPageData = that.accessor_listPages[pageID];
				$('.pageSection').html(that.renderModel(thisPageData, $(that.php_page_inner)));
				$('.pageSection').find('.the_content').after('<div class="blogListing" />');

				that.returnAndSaveJsonData('listBlogItems', function(blogData){
					_.each(blogData, function(value_blogData, index_blogData){
						var thisBlogView = that.renderModel(value_blogData, blogView.clone());

						thisBlogView.find('.commentForm').data('blogid', value_blogData.post_id);
						thisBlogView.addClass('blogItem' + value_blogData.post_id);

						var thisPostID = that.slugify(_.unescape(value_blogData.the_title)) + '-' + value_blogData.post_id;
						thisBlogView.find('.the_title').wrap( '<a class="blogLink" href="/' + pageID + '/' + thisPostID + '/"></a>' );
						thisBlogView.find('.blogLink').attr('data-pageid', pageID);
						thisBlogView.find('.blogLink').attr('data-postid', thisPostID);

						thisBlogView.css({
							'border-top': '12px solid black',
							'margin-top': '12px',
							'padding-top': '12px'
						});

						$('.blogListing').append(thisBlogView);
						
						processCommentForm(thisBlogView.find('.commentForm'), false, loadComments);

						loadComments(value_blogData.post_id);
					});

					$('.blogLink').on('click', function(e){
						e.preventDefault();
						that.pushPageNav($(this).attr('data-pageid'), $(this).attr('data-postid'))
					})
				});

				that.changePage();
			});
		};

		var processCommentForm = function (theForm, doCaptcha, callback){
			var doCaptcha = (typeof doCaptcha !== "undefined") ? doCaptcha : false;
			var csvUpdate = (theForm.data('csvupdate') != "") ? theForm.data('csvupdate') : 'formData';

			// you must enter your public key here - https://www.google.com/recaptcha/
			if(doCaptcha){
				Recaptcha.create("6LcgYfASAAAAAAvTvfb5r97NKTNnMbnCUnVN2-bp", theForm.find('.captchaField').attr('id'), {
					tabindex: 1,
					theme: "clean",
					callback: Recaptcha.focus_response_field
				});
			}

			function sendForm(){
				var blogID = theForm.data('blogid');
				$.post(that.pageDir + "/machines/handlers/processCommentForm.php", { formData: formData, blogID: blogID, csvUpdate: csvUpdate }, function(data) {
					callback(blogID);
					if(data == "success"){
						$(".formResponse").html("Form sent!");
						// $(".formResponse").css('color', 'green');
						$(".formResponse").css('display', 'block !important');
	//					$('#contactCaptcha').find('.fieldResponse').html('');
					} else {
						$(".formResponse").html("Form not sent. Please refresh the page and try again");
	//					$(".formResponse").css('color', 'red');
						$(".formResponse").css('display', 'block !important');
					}
				});
			}
		

			theForm.validate({
				errorClass: "formError",
				submitHandler: function(thisForm) {
					formData = theForm.serialize();
					postArgs = {
						challenge: $('#recaptcha_challenge_field').val(),
						response: $('#recaptcha_response_field').val()				
					}

					// you must enter your private key in "/machines/libraries/recaptcha/recaptchaResponse.php" - https://www.google.com/recaptcha/
					if(doCaptcha){
						$.post(pageDir + "/machines/libraries/recaptcha/recaptchaResponse.php", postArgs, function(data){
							if(data.substring(0,4) == "true"){
								sendForm();
							} else {
								Recaptcha.reload()
								returnObject = $('<div class="fieldResponse">Incorrect Captcha, please try again</div>');
								$('#contactCaptcha').append(returnObject)
							}
						})
					} else {
						sendForm();
					}

				},
				invalidHandler: function(event, validator) {
					errors = validator.numberOfInvalids();
					if (errors) {
						message = errors == 1 ? 'You missed 1 field.' : 'You missed ' + errors + ' fields.';
	//					$(".formResponse").html(message);
						$(".formResponse").show();
					} else {
						$(".formResponse").hide();
					}
				},
				errorPlacement: function(error, element) {
					switch(element.attr("name")){
						case "spam":
							$("input[name='spam']").parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
						break;
						case "transportation":
							$("input[name='transportation']").parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
						break;
						case "subscribe_email":
							element.parents('fieldset').after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
						break;
						default:
							element.parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
					}
				},
				messages: {
					email: {
						email: "Your email address must be in the format of name@domain.com"
					},
					telephone: {
						phoneUS: "Your phone number must be in the format of 212-555-1000"
					},
				},
				rules: {
					email: {
						required: true,
						email: true
					},
					telephone:{
						required: false,
						phoneUS: true
					},
					comment:{
						required: true
					},
					subscribe_email:{
						required: true,
						email: true
					}
				}
			});
		}

		if(that.postIDnotSet()){
			renderBlogListing();
		} else {
			renderBlogItem();
		}

	}


})(jQuery);