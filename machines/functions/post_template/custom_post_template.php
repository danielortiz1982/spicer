<?php

// REGISTER CUSTOM POST TYPE
	add_action( 'init', 'register_post_type__modelSlug_');
	function register_post_type__modelSlug_(){

		$labels = array(
			'name' => '_modelPlural_',
			'singular_name' => '_modelSingular_',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New _modelSingular_',
			'edit_item' => 'Edit _modelSingular_',
			'new_item' => 'New _modelSingular_',
			'view_item' => 'View _modelSingular_',
			'search_items' => 'Search _modelPlural_',
			'not_found' => 'Nothing found',
			'not_found_in_trash' => 'Nothing found in trash',
			'parent_item_colon' => ''
		);

		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => null,
			'supports' => _supportArray_
		);

		register_post_type( '_modelSlug_', $args);

	}

// DEFINE META BOXES
	$_modelSlug_MetaBoxArray = array(_modelFields_);

// ADD META BOXES
	add_action( "admin_init", "admin_init__modelSlug_" );
	function admin_init__modelSlug_(){
		global $_modelSlug_MetaBoxArray;
		generateMetaBoxes($_modelSlug_MetaBoxArray);
	}

// SAVE POST TO DATABASE
	add_action('save_post', 'save__modelSlug_');
	function save__modelSlug_(){
		global $_modelSlug_MetaBoxArray;
		savePostData($_modelSlug_MetaBoxArray, $post, $wpdb);
	}

// SORTING CUSTOM SUBMENU

	add_action('admin_menu', 'register_sortable__modelSlug__submenu');

	function register_sortable__modelSlug__submenu() {
		add_submenu_page('edit.php?post_type=_modelSlug_', 'Sort _modelPlural_', 'Sort', 'edit_pages', '_modelSlug__sort', 'sort__modelSlug_');
	}

	function sort__modelSlug_() {
		
		echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
			echo '<h2>Sort _modelPlural_</h2>';
		echo '</div>';

		list_modelPluralSlug_('sort');
	}

// CUSTOM COLUMNS

	// add_action("manage_posts_custom_column",  "_modelSlug__custom_columns");
	// add_filter("manage_edit-_modelSlug__columns", "_modelSlug__edit_columns");

	// function _modelSlug__edit_columns($columns){
	// 	$columns = array(
	// 		"full_name" => "_modelSingular_ Name",
	// 	);

	// 	return $columns;
	// }
	// function _modelSlug__custom_columns($column){
	// 	global $post;

	// 	switch ($column) {
	// 		case "full_name":
	// 			$custom = get_post_custom();
	// 			echo "<a href='post.php?post=" . $post->ID . "&action=edit'>" . $custom["first_name"][0] . " " . $custom["last_name"][0] . "</a>";
	// 		break;
	// 	}
	// }

// LISTING FUNCTION
	function list_modelPluralSlug_($context, $idArray = null){
		global $post;
		global $_modelSlug_MetaBoxArray;
		
		switch ($context) {
			case 'sort':
				$args = array(
					'post_type'  => '_modelSlug_',
					'order'   => 'ASC',
					'meta_key'  => 'custom_order',
					'orderby'  => 'meta_value_num',
					'nopaging' => true
				);
				$loop = new WP_Query($args);

				echo '<ul class="sortable">';
				while ($loop->have_posts()) : $loop->the_post(); 
					$output = get_the_title($post->ID);//get_post_meta($post->ID, 'first_name', true) . " " . get_post_meta($post->ID, 'last_name', true);
					include(TEMPDIR . '/views/item_sortable.php');
				endwhile;
				echo '</ul>';
			break;
			
			case 'json':
				$args = array(
					'post_type'  => '_modelSlug_',
					'order'   => 'ASC',
					'meta_key'  => 'custom_order',
					'orderby'  => 'meta_value_num',
					'nopaging' => true
				);
				returnData($args, $_modelSlug_MetaBoxArray, 'json', '_modelSlug__data');
			break;

			case 'array':
				$args = array(
					'post_type'  => '_modelSlug_',
					'order'   => 'ASC',
					'meta_key'  => 'custom_order',
					'orderby'  => 'meta_value_num',
					'nopaging' => true,
					'post__in' => $idArray
				);
				return returnData($args, $_modelSlug_MetaBoxArray, 'array');
			break;

			case 'rest':
				$args = array(
					'post_type'  => '_modelSlug_',
					'order'   => 'ASC',
					'meta_key'  => 'custom_order',
					'orderby'  => 'meta_value_num',
					'nopaging' => true,
					'post__in' => $idArray
				);
				return returnData($args, $_modelSlug_MetaBoxArray, 'array');
			break;

			case 'inputs':
				$args = array(
					'post_type'  => '_modelSlug_',
					'order'   => 'ASC',
					'meta_key'  => 'custom_order',
					'orderby'  => 'meta_value_num',
					'nopaging' => true
				);

				$outputArray = returnData($args, $_modelSlug_MetaBoxArray, 'array');

				$field_options = array();
				foreach ($outputArray as $key => $value) {
					$checkBoxOption = array(
						"id" => $value['post_id'],
						"name" => html_entity_decode($value['the_title']),
					);
					$field_options[] = $checkBoxOption;
				}

				return $field_options;

			break;

		}
	}

?>