<?php

// DEFINE META BOXES
	$pageMetaBoxArray = array(
		"page_second_editor_meta" => array(
	    	"id" => "page_second_editor_meta",
	        "name" => "Second Editor",
	        "post_type" => "page",
	        "position" => "normal",
	        "priority" => "low",
	        "callback_args" => array(
	        	"input_type" => "input_editor",
	        	"input_name" => "second_editor"
	        )
	    )
	    // "page_featured_images_meta" => array(
	    // 	"id" => "page_featured_images_meta",
	    //     "name" => "Featured Images",
	    //     "post_type" => "page",
	    //     "position" => "side",
	    //     "priority" => "low",
	    //     "callback_args" => array(
	    //     	"input_type" => "input_featured_images",
	    //     	"input_name" => "page_featured_images"
	    //     )
	    // ),
	    // "page_sample_text_meta" => array(
	    // 	"id" => "page_sample_text_meta",
	    //     "name" => "Sample Text",
	    //     "post_type" => "page",
	    //     "position" => "side",
	    //     "priority" => "low",
	    //     "callback_args" => array(
	    //     	"input_type" => "input_text",
	    //     	"input_name" => "sample_text"
	    //     )
	    // ),
	    // "page_sample_date_meta" => array(
	    // 	"id" => "page_sample_date_meta",
	    //     "name" => "Sample Date",
	    //     "post_type" => "page",
	    //     "position" => "side",
	    //     "priority" => "low",
	    //     "callback_args" => array(
	    //     	"input_type" => "input_date",
	    //     	"input_name" => "sample_date"
	    //     )
	    // ),
	    // "page_sample_color_meta" => array(
	    // 	"id" => "page_sample_color_meta",
	    //     "name" => "Sample Color",
	    //     "post_type" => "page",
	    //     "position" => "side",
	    //     "priority" => "low",
	    //     "callback_args" => array(
	    //     	"input_type" => "input_colorpicker",
	    //     	"input_name" => "sample_color",
	    //     	"input_palette" => array(
	    //     		'rgb(0, 59, 168);',
	    //     		'rgb(102, 153, 51);',
					// 'rgb(53, 109, 211);',
					// 'rgb(95, 136, 211);',
	    //     	)
	    //     )
	    // ),
	    // "page_sample_editor_meta" => array(
	    // 	"id" => "page_sample_editor_meta",
	    //     "name" => "Sample Editor",
	    //     "post_type" => "page",
	    //     "position" => "side",
	    //     "priority" => "low",
	    //     "callback_args" => array(
	    //     	"input_type" => "input_editor",
	    //     	"input_name" => "sample_editor"
	    //     )
	    // ),
	    // "page_sample_checkbox_multi_meta" => array(
	    // 	"id" => "page_sample_checkbox_multi_meta",
	    //     "name" => "Sample Checkbox Multi",
	    //     "post_type" => "page",
	    //     "position" => "side",
	    //     "priority" => "low",
	    //     "callback_args" => array(
	    //     	"input_type" => "input_checkbox_multi",
	    //     	"input_source" => "listPages",
	    //     	"input_name" => "sample_checkbox_multi"
	    //     )
	    // ),
	    // "people_sample_radio_meta" => array(
	    // 	"id" => "people_sample_radio_meta",
	    //     "name" => "Sample Radio",
	    //     "post_type" => "page",
	    //     "position" => "side",
	    //     "priority" => "low",
	    //     "callback_args" => array(
	    //     	"input_type" => "input_radio",
	    //     	"input_source" => "listPages",
	    //     	"input_name" => "sample_radio"
	    //     )
	    // ),
	    // "page_sample_select_meta" => array(
	    // 	"id" => "page_sample_select_meta",
	    //     "name" => "Sample Select",
	    //     "post_type" => "page",
	    //     "position" => "side",
	    //     "priority" => "low",
	    //     "callback_args" => array(
	    //     	"input_type" => "input_select",
	    //     	"input_source" => "listPages",
	    //     	"input_name" => "sample_select"
	    //     )
	    // ),
	    // "page_sample_checkbox_single_meta" => array(
	    // 	"id" => "page_sample_checkbox_single_meta",
	    //     "name" => "Sample Checkbox Single",
	    //     "post_type" => "page",
	    //     "position" => "side",
	    //     "priority" => "low",
	    //     "callback_args" => array(
	    //     	"input_type" => "input_checkbox_single",
	    //     	"input_name" => "page_sample_checkbox_single",
	    //     	"input_text" => "Sample Option"
	    //     )
	    // ),
	    // "page_sample_hidden_meta" => array(
	    // 	"id" => "page_sample_hidden_meta",
	    //     "name" => "Sample Hidden",
	    //     "post_type" => "page",
	    //     "position" => "side",
	    //     "priority" => "low",
	    //     "callback_args" => array(
	    //     	"input_type" => "input_hidden",
	    //     	"input_name" => "page_sample_hidden"
	    //     )
	    // ),


	);

// ADD META BOXES
	add_action( "admin_init", "admin_init_page" );
	function admin_init_page(){
		global $pageMetaBoxArray;
		generateMetaBoxes($pageMetaBoxArray);
	}

// SAVE POST TO DATABASE
	add_action('save_post', 'save_page');
	function save_page(){
		global $pageMetaBoxArray;
		savePostData($pageMetaBoxArray, $post, $wpdb);
	}

// LISTING FUNCTION
	function listPages($context, $idArray = null){
		global $post;
		global $pageMetaBoxArray;
		
		switch ($context) {
			case 'sort':
				$args = array(
					'post_type'  => 'page',
					'order'   => 'ASC',
					'nopaging' => true
				);
				$loop = new WP_Query($args);

				echo '<ul class="sortable">';
				while ($loop->have_posts()) : $loop->the_post(); 
					$output = get_the_title($post->ID);//get_post_meta($post->ID, 'first_name', true) . " " . get_post_meta($post->ID, 'last_name', true);
					include(TEMPDIR . '/views/item_sortable.php');
				endwhile;
				echo '</ul>';
			break;
			
			case 'json':
				$args = array(
					'post_type'  => 'page',
					'order'   => 'ASC',
					'nopaging' => true
				);
				returnData($args, $pageMetaBoxArray, 'json', 'page_data');
			break;

			case 'array':
				$args = array(
					'post_type'  => 'page',
					'order'   => 'ASC',
					'nopaging' => true
				);
				return returnData($args, $pageMetaBoxArray, 'array');
			break;

			case 'rest':
				$args = array(
					'post_type'  => 'page',
					'order'   => 'ASC',
					'nopaging' => true,
					'post__in' => $idArray
				);
				return returnData($args, $pageMetaBoxArray, 'array');
			break;


			case 'inputs':
				$args = array(
					'post_type'  => 'page',
					'order'   => 'ASC',
					'nopaging' => true
				);

				$outputArray = returnData($args, $peopleMetaBoxArray, 'array');

				$field_options = array();
				foreach ($outputArray as $key => $value) {
					$checkBoxOption = array(
						"id" => $value['post_id'],
						"name" => html_entity_decode($value['the_title']),
					);
					$field_options[] = $checkBoxOption;
				}

				return $field_options;

			break;

		}
	}

?>