## Local Setup
----
#### Copy this readme, find and replace the following terms and then [use a markdown editor](http://markdownlivepreview.com/) to complete the rest of the document
	YOUR_GITLAB_TOKEN 
	YOUR_DIGITAL_OCEAN_KEY 
	NEW_WEBSITE_NAME.COM 
	NEW_USERNAME 
	GITLAB_USER@DOMAIN.COM 
	NEW_THEME_NAME 
	YOUR_MACHINE 
	YOUR_URL 
	YOUR_USERNAME 
	YOUR_PASSWORD 

#### Setup variables (local)

	echo export GITLAB_TOKEN=YOUR_GITLAB_TOKEN >> ~/.bash_profile 
	echo export DigO_TOKEN=YOUR_DIGITAL_OCEAN_KEY >> ~/.bash_profile
	source ~/.bash_profile

#### Create an ssh key for your device and then log in and add to [GitLab](http://www.gitlab.com)
	ssh-keygen -t rsa -C "GITLAB_USER@DOMAIN.COM"
	cat ~/.ssh/id_rsa.pub
	curl -X POST "https://gitlab.com/api/v3/user/keys" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" -H "Content-Type: application/json" -d'{"title":"YOUR_MACHINE-computer-key","key":"THE_KEY_THAT_YOU_JUST_MADE"}'

## Server Setup
----
### Create a new server (local)
    curl -X POST "https://api.digitalocean.com/v2/droplets" -H "Authorization: Bearer $DigO_TOKEN" -H "Content-Type: application/json" -d'{"name":"NEW_WEBSITE_NAME.COM","region":"nyc3","size":"1gb","image":"wordpress","backups":"true"}'
### Create a new user (server)
	adduser NEW_USERNAME
#### Modify the user's permissions
	sudo adduser NEW_USERNAME www-data
	sudo chown -R www-data:www-data /var/www/html
	sudo chmod -R g+w /var/www/html
	visudo
>
	NEW_USERNAME ALL=(ALL:ALL) ALL

### Restart server and exit (server)
	sudo service apache2 restart
	exit

### Install git as non root user (server)
	sudo apt-get update
	sudo apt-get install git

## Repo Setup
----
### Create keys
>#### Generate key  (server)
	sudo chown -R www-data:www-data /var/www
	sudo chmod -R g+w /var/www
	sudo -u www-data ssh-keygen -t rsa -C "GITLAB_USER@DOMAIN.COM"
	sudo -u www-data cat /var/www/.ssh/id_rsa.pub 
#### Add key to Gitlab (local)
	curl -X POST "https://gitlab.com/api/v3/user/keys" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" -H "Content-Type: application/json" -d'{"title":"NEW_THEME_NAME-server-key","key":"THE_KEY_THAT_YOU_JUST_MADE_ON_THE_SERVER"}'

### Create the repo on Gitlab (local)
	curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" https://gitlab.com/api/v3/projects --data "name=NEW_THEME_NAME&namespace_id=126061"
### Copy boilerplate repo into new directory (server)
	git -C /var/www/html/wp-content/themes/ clone https://gitlab.com/bermangrp/boilerplate-wordpress.git
	rm -rf /var/www/html/wp-content/themes/boilerplate-wordpress/.git
	mv /var/www/html/wp-content/themes/boilerplate-wordpress/ /var/www/html/wp-content/themes/NEW_THEME_NAME-dev/
	sudo chown -R www-data:www-data /var/www/html/wp-content/themes/NEW_THEME_NAME-dev/machines/handlers/git
	sudo chmod -R g+w /var/www/html/wp-content/themes/NEW_THEME_NAME-dev/machines/handlers/git/

### Download the directory and push to repo (local)
	mkdir /Users/YOUR_MACHINE/Desktop/optionalContainer
	scp -r NEW_USERNAME@YOUR_URL:/var/www/html/wp-content/themes/NEW_THEME_NAME-dev /Users/YOUR_MACHINE/Desktop/optionalContainer
	cd /Users/YOUR_MACHINE/Desktop/optionalContainer/NEW_THEME_NAME-dev/
	subl /Users/YOUR_MACHINE/Desktop/optionalContainer/NEW_THEME_NAME-dev/
>
	"save_before_upload": true,
	"upload_on_save": true,
	"sync_down_on_open": false,
	"sync_skip_deletes": false,
	"sync_same_age": true,
	"confirm_downloads": false,
	"confirm_sync": true,
	"confirm_overwrite_newer": false,
>
	"host": "YOUR_URL",
	"user": "YOUR_USERNAME",
	"password": "YOUR_PASSWORD",
	//"port": "22",
>
	"remote_path": "/var/www/html/wp-content/themes/NEW_THEME_NAME-dev",
	
	git init
	git add -A
	git commit -m "first commit"
	git remote add origin git@gitlab.com:bermangrp/NEW_THEME_NAME.git
	git push -u origin master
	curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" https://gitlab.com/api/v3/projects/bermangrp%2FNEW_THEME_NAME/hooks --data "url=http://YOUR_URL/wp-content/themes/NEW_THEME_NAME-dev/machines/handlers/git/gitWebHook.php"

### Clone the repo to the production environment (server)
	sudo -u www-data git -C /var/www/html/wp-content/themes/ clone git@gitlab.com:bermangrp/NEW_THEME_NAME.git


## Wordpress Setup
----
### Install theme (browser)
* finish wordpress server setup
* activate theme from wp-admin (hint: hover over "Live Preview" to see which theme has the "-dev" in its URL path and be sure NOT to use that one)
* in Pages, change Template of "Sample Page" to Home
* create a menu called "Main Menu", add "Sample Page" to the menu, and assign it to "Header Menu"
* in Settings->Reading change Front Page to "Sample Page"
* in Permalinks change Common Settings to Default

### Set up theme (local)
* upload .htaccess to /var/www/html (if permission denied you may have to reset permission on this folder with: sudo chown -R www-data:www-data /var/www/html; sudo chmod -R g+w /var/www/html;)

>
	RewriteEngine On
	RewriteBase /
	RewriteRule ^index\.php$ - [L]
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteRule . index.php [L]

* modify dynamics.js

>
	themeFolderName: 'NEW_THEME_NAME',

* modify /wp-content/themes/NEW\_THEME\_NAME-dev/machines/handlers/git/gitWebHook.php

>
	shell_exec('/usr/bin/git -C /var/www/html/wp-content/themes/NEW_THEME_NAME pull git@gitlab.com:bermangrp/NEW_THEME_NAME.git');

### Test webhook
>* go to "YOUR\_URL" and also to "YOUR_URL/?dev" to see the two different sites (the main site should not be rendering because it doesn't have the change to dynamics.js yet)
* go to YOUR\_URL/wp-content/themes/NEW\_THEME\_NAME-dev/machines/handlers/git/gitWebHook.php and check to see that a test file "gitWebHook-test" was created in same folder
>
#### Push repo changes to gitlab (local)
	git add -A
	git commit -m "webhooks"
	git push -u origin master
#### Check server
* if you don't see "the\_content" at YOUR\_URL/ but you do see it on YOUR\_URL/?dev, then log into the server and run the Web Hook file as the www-data user using this command: 

>
		sudo -u www-data /usr/bin/git -C /var/www/html/wp-content/themes/NEW_THEME_NAME pull git@gitlab.com:bermangrp/NEW_THEME_NAME.git