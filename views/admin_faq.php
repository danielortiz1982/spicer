<h1>FAQ</h1>
<style>
	.faq_container ul{
		padding-left: 15px;
	}
	.faq_item{
		background-color: rgb(230, 230, 230);
		padding: 10px 0px;
	}
</style>
<div class="faq_container">
	<ul>
		<li>
			<h3>Sitewide</h3>
			<div class="faq_item">
				<ul>
					<li><h4>For changes to page titles, the menu, header, footer, styling, and/or functionality, please contact The Berman Group</h4></li>
				</ul>
			</div>
		</li>
		<li>
			<h3>
				<a target="_blank" href="http://www.spicerh2hinspections.com/">Home</a>
			</h3>
			<div class="faq_item">
				<ul>
					<li>
						<a target="_blank" href="http://www.spicerh2hinspections.com/wp-admin/post.php?post=6&action=edit">Edit page main text</a>
					</li>
					<li>
						<a target="_blank" href="http://www.spicerh2hinspections.com/wp-admin/edit.php?post_type=home_slides">Edit slideshow image or text</a>
					</li>
					<li>
						<a target="_blank" href="http://www.spicerh2hinspections.com/wp-admin/post.php?post=6&action=edit">Edit service areas text</a>
					</li>
					<li>
						<a target="_blank" href="http://www.spicerh2hinspections.com/wp-admin/edit.php?post_type=links">Edit links </a>
					</li>
					<li>
						<a target="_blank" href="http://www.spicerh2hinspections.com/wp-admin/post-new.php?post_type=links">Add new links </a>
					</li>	
					<li>
						<a target="_blank" href="http://www.spicerh2hinspections.com/wp-admin/edit.php?post_type=testimonials">Edit Testimonials</a>
					</li>
					<li>
						<a target="_blank" href="http://www.spicerh2hinspections.com/wp-admin/post-new.php?post_type=testimonials">Add new testimonial </a>
					</li>																														
				</ul>
			</div>
		</li>
<!-- 		<li>
			<h3>
				<a target="_blank" href="http://104.236.67.21/">Company</a>
			</h3>
			<div class="faq_item">
				<ul>
					<li>
						<a target="_blank" href="http://104.236.67.21/wp-admin/post.php?post=2&action=edit">Edit page text or page image</a>
					</li>
				</ul>
			</div>
		</li> -->
		<li>
			<h3>
				<a target="_blank" href="http://www.spicerh2hinspections.com/about/">About</a>
			</h3>
			<div class="faq_item">
				<ul>
					<li>
						<a target="_blank" href="http://www.spicerh2hinspections.com/wp-admin/post.php?post=9&action=edit">Edit page text or page image</a>
					</li>
					<li>
						<a target="_blank" href="http://www.spicerh2hinspections.com/wp-admin/edit.php?post_type=qualifications">Edit qualifications</a>
					</li>	
					<li>
						<a target="_blank" href="http://www.spicerh2hinspections.com/wp-admin/post-new.php?post_type=qualifications">Add new qualifications</a>
					</li>										
				</ul>
			</div>
		</li>
		<li>
			<h3>
				<a target="_blank" href="http://www.spicerh2hinspections.com/services/">Services</a>
			</h3>
			<div class="faq_item">
				<ul>
					<li>
						<a target="_blank" href="http://www.spicerh2hinspections.com/wp-admin/post.php?post=11&action=edit">Edit page text or page image</a>
					</li>				
				</ul>
			</div>
		</li>
		<li>
			<h3>
				<a target="_blank" href="http://www.spicerh2hinspections.com/contact/">Contact</a>
			</h3>
			<div class="faq_item">
				<ul>
					<li>
						<a target="_blank" href="http://www.spicerh2hinspections.com/wp-admin/post.php?post=13&action=edit">Edit page text or page image</a>
					</li>
				</ul>
			</div>
		</li>
	</ul>	
</div>