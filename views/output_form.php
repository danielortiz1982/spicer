<div class="formWrapper">
	<form class="commentForm" method="get" action="">
		<fieldset>
			<div class="formField">
				<div class="fieldContent">
					<label for="title">Title</label>
					<input type="text" class="title form-control" name="title" />
				</div>
			</div>
			<div class="formField">
				<div class="fieldContent">
					<label for="telephone">Telephone</label>
					<input type="text" class="telephone form-control" name="telephone" />
				</div>
			</div>
			<div class="formField">
				<div class="fieldContent">
					<label for="email">E-Mail</label>
					<input type="text" class="email form-control" name="email" />
				</div>
			</div>
			<div class="formField submitField">
				<div class="fieldContent">
					<label for="sample_select">Sample Select</label>
					<div class="select-wrapper">
						<select class="sample_select form-control" name="sample_select">
							<option value="schedule">Schedule an inspection</option>
							<option value="information">Get more information</option>
						</select>
					</div>
				</div>
			</div>
			<div class="formField captchaField" id="sampleFormCaptcha"></div>
			<div class="formField">
				<input class="submit btn btn-primary" type="submit" value="Submit"/>
			</div>
		</fieldset>
		<div class="formResponse"></div>
	</form>
</div>