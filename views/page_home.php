<div class="container_home">
	<div class="container_slideshow">
		<div class="homeSlides"></div>
	</div>
	<div class="container">
		<div class="page_row">
			<div class="page_column">
				<div class="row_box">
					<div class="the_content"></div>
					<div class="linkListbox">
						<div class="linkListing"></div>
						<div class="second_editor_box"><div class="second_editor"></div></div>
					</div>
					
				</div>
			</div>
		</div>
		<div class="page_row">
			<div class="carouselContainer"></div>
			<div class="homeBoxes"></div>
		</div>
	</div>
</div>