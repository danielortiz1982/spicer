<!DOCTYPE html>
<html lang="en" class="<?php echo returnBrowser(); ?>">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title><?php bloginfo('name');?></title>

        <?php wp_head(); ?>

        <link rel='stylesheet' type='text/css' href='<?php echo PAGEDIR; ?>/machines/libraries/mmenu/jquery.mmenu.all.css' />
        <link rel="stylesheet" type="text/css" href="<?php echo PAGEDIR; ?>/machines/libraries/owlcarousel/owlcarousel.css">
        <link rel="stylesheet" type="text/css" href="<?php echo PAGEDIR; ?>/styles/styles.min.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body data-tempdir="<?php echo PAGEDIR; ?>" id="<?php echo get_post( $post )->post_name; ?>" <?=(returnMobile() == 'true')? 'class="mobileMode"' : 'class="desktopMode"';?>>

        <!-- POPULATE JAVASCRIPT VARIABLES -->
            <div>
                <?php 
                    generatePagesJSON(get_the_ID());
                    populateJavascript(returnMobile(), 'mobile');

                    if(DEVMODE){
                        populateJavascript('true', 'dev_mode');
                    } else {
                        populateJavascript('false', 'dev_mode');
                    }

                    if(isset($_GET['url'])){
                        populateJavascript('true', 'dev_refresh');
                    } else {
                        populateJavascript('false', 'dev_refresh');
                    }

                    if ( is_user_logged_in() ) {
                        populateJavascript('true', 'user_logged_in');

                        $userdat = wp_get_current_user();
                        $allUserData = listUsers('array');

                        foreach ($allUserData as $key => $value) {
                            if($value['id'] == $userdat->data->ID){
                                $userData = $value;
                            }
                        }

                        populateJSON($userData, 'user_data');
                    } else {
                        populateJavascript('false', 'user_logged_in');
                    }            
                ?>
            </div>

        <!-- HEADER -->
            <nav class="navbar navbar-default navbar-fixed-top header">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo home_url(); ?>">
                            <img src="<?php echo PAGEDIR; ?>/images/graphics/logo.png" alt="<?php bloginfo('name');?>" />
                        </a>
                    </div>
                    <?php 

                    class bootstrapDropDown extends Walker_Nav_Menu {
                        function start_lvl( &$output, $depth = 0, $args = array() ) {
                            $indent = str_repeat("\t", $depth);
                            $output .= "\n$indent<ul class=\"sub-menu dropdown-menu\">\n";
                        }
                    }

                    $defaults = array(
                        'theme_location'  => 'header-menu',
                        'menu'            => '',
                        'container'       => 'div',
                        'container_class' => 'collapse navbar-collapse',
                        'container_id'    => 'navbar',
                        'menu_class'      => 'nav navbar-nav',
                        'menu_id'         => '',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'before'          => '',
                        'after'           => '',
                        'link_before'     => '<span>',
                        'link_after'      => '</span>',
                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth'           => 0,
                        'walker'          => new bootstrapDropDown()
                        );
                    wp_nav_menu( $defaults );
                    ?>
                </div>
            </nav>
            <div class="featured_img_box"><div class="featured_img"></div></div>
            
        <!-- PAGE CONTENT -->
            <section class="pageSection">
                
            </section>

        <!-- FOOTER -->
            <footer class="footer">
                <div class="container">
                    <div class="footerInfo col-md-4 col-sm-12 col-xs-12">
                        <div class="displayTable">
                            <div class="displayTableCell">
                                <p>
                                    House 2 Home Inspections, Inc.<br />
                                    314 10th Street, Brooklyn, NY 11215<br />
                                    917 887 8344<br />
                                    <a href="mailto:espice314@gmail.com">espice314@gmail.com</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="paymentMenu col-md-4 col-sm-12 col-xs-12">
                        <div class="displayTable">
                            <div class="displayTableCell">
                                <ul>
                                    <li>
                                         <span class="fa fa-cc-mastercard"></span>
                                    </li>
                                    <li>
                                        <span class="fa fa-cc-visa"></span>
                                    </li>
                                  
                                </ul>
                            </div>
                        </div>
                    </div>                    
                    <div class="footerMenu col-md-4 col-sm-12 col-xs-12">
                        <div class="displayTable footerWidth">
                            <div class="displayTableCell">
                                <ul>
                                    <li>
                                        <a href="/home/">Home</a>
                                    </li>
                                    <li>
                                        <a href="/about/">About</a>
                                    </li>
                                    <li>
                                        <a href="/services/">Services</a>
                                    </li>
                                    <li>
                                        <a href="/contact/">Contact</a>
                                    </li>                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            
        <!-- SCRIPTS -->
            <div>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/owlcarousel/owlcarousel.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/modernizr/modernizr.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/backstretch/backstretch.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/validate/validate.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/boilerplate/boilerplate.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/columnizer/columnizer.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/mmenu/jquery.mmenu.min.all.js"></script>
                <script src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry&v=3.7"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/maplace/maplace.min.js"></script>
                <script src="<?php echo PAGEDIR; ?>/machines/libraries/textFit/jquery.textFit.slow.js"></script>
                <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
                
                  ga('create', 'UA-76775249-1', 'auto');
                  ga('send', 'pageview');
                
                </script>
                <?php if (returnBrowser() !== "internet-explorer-8"): ?>
                    <script src="<?php echo PAGEDIR; ?>/machines/libraries/dynamics/dynamics.js"></script>
                <?php endif; ?>
                <?php wp_footer(); ?>
            </div>
    </body>
</html>