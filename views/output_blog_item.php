<div class="blogItem">
	<div class="the_title"></div>
	<div class="the_content"></div>
	<div class="comments">
		<div class="commentList">
			<div class="commentItem">
				<div class="comment_author_email"></div>
				<div class="comment_date"></div>
				<div class="comment_content"></div>
			</div>
		</div>
		<div class="commentFormBlock">
			<form class="commentForm" method="get" action="">
				<fieldset>
					<div class="formField">
						<div class="fieldContent">
							<label for="comment">Comment</label>
							<textarea class="comment" name="comment"></textarea>
						</div>
					</div>
					<div class="formField">
						<div class="fieldContent">
							<label for="email">E-Mail</label>
							<input type="text" class="email" name="email" value="test@test.com" />
						</div>
					</div>
					<div class="formField captchaField" id="sampleFormCaptcha"></div>
					<div class="formField">
						<input class="submit" type="submit" value="Submit"/>
					</div>
				</fieldset>
				<div class="formResponse"></div>
			</form>
		</div>
	</div>
</div>