<form name="loginform">
	<h2>Log In To Continue</h2>
	<fieldset>
		<div class="formField">
			<div class="fieldContent">
				<label for="user_login">Username<br>
				<input type="text" name="user_login" id="user_login" class="input" value="" size="20"></label>
			</div>
		</div>
		<div class="formField">
			<div class="fieldContent">
				<label for="user_pass">Password<br>
				<input type="password" name="user_pass" id="user_pass" class="input" value="" size="20"></label>
			</div>
		</div>
	</fieldset>
	<fieldset class="submitFieldSet">
		<div class="formField">
			<input type="submit" name="submit" id="submit" class="button submit" value="Log In">
		</div>
	</fieldset>
	<div class="linkedInSubmit formField">
		<span>OR</span>
		<div>
			<script type="text/javascript" src="http://platform.linkedin.com/in.js">
			  api_key: YOUR_API_KEY_GOES_HERE
			  onLoad: linkedInLogin
			</script>
			<script type="in/Login"></script>
		</div>
	</div>
</form>
