<?php
	require_once realpath(dirname(__FILE__).'/..').'/wp-load.php';

	class BaseClass {
		private $vars = array(
			'baseURL' => '/~illatoz/onepage/_data/',
			'routes' => array(
				'people' => 'listPeople',
			),
		);

		public function __construct() {
			$this->parseURL();
			// $this->debug();
		}

		private function parseURL(){
			$requestURI = explode("/", str_replace($this->vars['baseURL'], '', $_SERVER['REQUEST_URI']));
			switch ($requestURI[0]) {			
				default:
					$this->vars['request'] = $requestURI[0];
					$this->vars['post_id'] = $requestURI[1];
					$this->vars['post_var'] = $requestURI[2];

					$this->routeURL();
				break;
			}
		}

			private function routeURL(){
				$returnData;
				foreach ($this->vars['routes'] as $key => $value) {
					switch ($this->vars['request']) {
						case $key:
							switch ($this->vars['post_id']) {
								case 'new':
									if(is_user_logged_in()){
										$postURL = $this->vars['baseURL'] . $this->vars['request'];
										echo $postURL;
//										include get_template_directory() . '/views/input_form.php';
										
										// $formInputs = array(
										// 	'post_title' => 'text',
										// );

										// $formOutput = '';
										// $formOutput .= '<form method="post" action="'.$postAction.'">';
										// foreach ($formInputs as $key => $value) {
										// 	switch ($value) {
										// 		case 'text':
										// 			ob_start();
										// 			$inputID = $key;
										// 			$inputValue = '';
										// 			include get_template_directory() . '/views/input_text.php';
										// 			$formOutput .= ob_get_clean();
										// 			break;
												
										// 		default:
										// 			# code...
										// 			break;
										// 	}
										// }

										// $formOutput .= '<p><input type="submit" name="submit" id="submit" value="Submit" /></p>';
										// echo $formOutput;

									} else {
										echo "you must log in";
									}
								break;

								case '':
									if(empty($_POST)){
										$this->returnJSON(call_user_func($value, 'array'));
									} else {
										if(is_user_logged_in()){

											$newPost = array(
											  'post_title'    => $_POST['post_title'],
											  'post_name' => $this->slugify($_POST['post_title']),
											  'post_content'  => 'This is my post.',
											  'post_status'   => 'publish',
											  'post_type' => $this->vars['request'],
											  'post_author' => get_current_user_id()
											);

											// Insert the post into the database
											if ( $new_post_ID = wp_insert_post( $newPost ) ) {
												// update_post_meta($new_post_ID, "custom_order", '-1');
												$this->returnJSON(call_user_func($value, 'array'));
											} else {
												echo 'Post could not be publish';
											}
										}
									}
								break;
								
								default:
									$jsonData = call_user_func($value, 'array');
									foreach ($jsonData as $key1 => $value1) {
										if($this->vars['post_id'] == $value1['post_id']){
											switch ($this->vars['post_var']) {
												case '':
													$this->returnJSON($value1);
												break;
												
												default:
													$this->returnJSON($value1[$this->vars['post_var']]);
												break;
											}
										}
									}
								break;
							}
						break;
						
						case "":
							foreach ($this->vars['routes'] as $key => $value) {
								echo "<a href='" . $key . "/'>" . $key . "</a><br/>";
							}
						break;
					}


				}
			}

			private function returnJSON($dataSent){
				header('Content-type: application/json');
				echo json_encode($dataSent, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);		
			}

			private function debug(){
				echo "<pre>";
				print_r($this->vars);
				echo "</pre>";
			}

			private function echoPre($someCode){
				echo "<pre>";
				print_r($someCode);
				echo "</pre>";
			}

			private function slugify($string) {
				$slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
	   			return $slug;
			}

			private function recursive_array_search($needle,$haystack) {
				foreach($haystack as $key=>$value) {
					$current_key = $key;
					if(is_string($value)){
						if (strpos(strtolower($value), strtolower($needle)) !== false){
							$resO = true;
						} else {
							$resO = false;
						}
					} else {
						$resO = false;
					}

					if (strpos(strtolower($key), strtolower($needle)) !== false){
						if(strpos(strtolower($value), 'true') !== false){
							$resO = true;
						}
					}

					if($resO OR (is_array($value) && $this->recursive_array_search($needle,$value) !== false)) {
						return true;
					}
				}
				return false;
			}
		}

		$obj = new BaseClass();

// RETIRED
	// new post
		// global ${$this->vars['request'] . "MetaBoxArray"};
		// global ${$this->vars['request'] . "SupportsInput"};
		// $formOutput = "";
		// foreach (${$this->vars['request'] . "SupportsInput"} as $key1 => $value1) {
		// 	switch ($value1) {
		// 		case 'title':
		// 			$inputID = "the_title";
		// 			$inputValue = "Title";
		// 			ob_start();
		// 			include(get_template_directory() . '/views/input_text.php');
		// 			$formOutput .= ob_get_clean();
		// 		break;
				
		// 		case 'editor':
		// 			# code...
		// 		break;
				
		// 		case 'thumbnail':
		// 			# code...
		// 		break;
		// 	}
		// }
		// $metaBoxLoop = ${$this->vars['request'] . "MetaBoxArray"};


		// // $this->echoPre(${$this->vars['request'] . "MetaBoxArray"});
		// include('views/form.php');
		// foreach ($metaBoxLoop as $key2 => $value2) {
		// 	$value2['args'] = $value2['callback_args'];
		// 	unset($value2['callback_args']);
		// 	populateMetaBoxes('', $value2);
		// }
