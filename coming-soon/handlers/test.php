<?php

    if( isset($_POST['formData']) ){

        $formData = $_POST['formData'];

        parse_str($formData, $formArray);

        $csvReturn = createCsv($formArray);
        sendAsEmail($formArray);

        echo writeToCsv($csvReturn);
    }

    class BaseClass {
        private $vars = array(
            'baseURL' => '/~illatoz/onepage/_data/',
            'routes' => array(
                'people' => 'listPeople',
            ),
        );

        public function __construct() {
            $this->parseURL();
            // $this->debug();
        }

        private function parseURL(){
            $requestURI = explode("/", str_replace($this->vars['baseURL'], '', $_SERVER['REQUEST_URI']));
            switch ($requestURI[0]) {           
                default:
                    $this->vars['request'] = $requestURI[0];
                    $this->vars['post_id'] = $requestURI[1];
                    $this->vars['post_var'] = $requestURI[2];

                    $this->routeURL();
                break;
            }
        }


    private function sendAsEmail($emailObject){

        ob_start();
        echo "<pre>";
            print_r($emailObject);
        echo "</pre>";
        $message = ob_get_clean();


        $message = '<html><body>';
        $message .= $messageText;

        $ccString = "CC: ssosa@bermangrp.com" . "\r\n";
        $ccString .= "BCC: coneal@bermangrp.com" . "\r\n";

        $to = 'rthompson@bermangrp.com';
        $subject = 'Contact Form Submission';
        $headers = "";
        // $headers = "From: " . strip_tags($_POST['req-email']) . "\r\n";
        // $headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
        // $headers .= $ccString;
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $message .= '</body></html>';

        mail($to, $subject, $message, $headers);
    }

    private function createCsv($formData){
        $csvReturn = "";
        $excludeKeys = array('recaptcha_challenge_field', 'recaptcha_response_field');
        foreach ($formData as $key => $value) {
            if(is_array($value)){
                foreach ($value as $innerKey => $innerValue) {
                    if(!in_array($key, $excludeKeys)) {
                        $csvSafeValue = str_replace(",", "", $innerValue);
                        $csvReturn .= $csvSafeValue . ",";
                    }
                }
            } else {
                if(!in_array($key, $excludeKeys)) {
                    $csvSafeValue = str_replace(",", "", $value);
                    $csvReturn .= $csvSafeValue . ",";
                }
            }
        }

        $csvReturn = "\n" . substr($csvReturn, 0, -1);

        return $csvReturn;
    }

    private function writeToCsv($fileContents){
        $fp = fopen($_SERVER['DOCUMENT_ROOT'] . '/coming-soon/handlers/comingSoon.csv', 'a');
        
        if ( fwrite($fp, $fileContents) ) {
            $fileStatus = "success";
        } else {
            $fileStatus = "fail";
        }

        fclose($fp);

        return $fileStatus;
    }

    private function slugify($text){
        $text = strtolower($text);
        $text = preg_replace('/\+/', '', $text); // Replace spaces with 
        $text = preg_replace('/\s+/', '-', $text); // Replace spaces with -
        $text = preg_replace('/[^\w\-]+/', '', $text); // Remove all non-word chars
        $text = preg_replace('/\-\-+/', '-', $text); // Replace multiple - with single -
        $text = preg_replace('/^-+/', '', $text); // Trim - from start of text
        $text = preg_replace('/-+$/', '', $text); // Trim - from end of text
        return $text;
    }


    }



?>
