<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>House2Home</title>

    <!-- Bootstrap Core CSS -->
    <link href="http://104.236.35.255/wp-content/themes/spicer-dev/coming-soon/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Custom CSS -->
    <link href="http://104.236.35.255/wp-content/themes/spicer-dev/coming-soon/css/stylish-portfolio.css" rel="stylesheet">
    <link href="http://104.236.35.255/wp-content/themes/spicer-dev/coming-soon/css/extracss.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="http://104.236.35.255/wp-content/themes/spicer-dev/coming-soon/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>



    <!-- Header -->
    <header id="top" class="header myheader">
        <div class="text-vertical-center">
            <div class="maintextbg">
                <h1>Coming Soon</h1>
                <h2>House2Home</h2>
                <h3>Contact us</h3>
                <div class="form">
                    <form action="index.php" method="POST">
                        <fieldset>
                            <input class="u-full-width email" type="email" name="email" placeholder="Your email here..." id="email">
                            <input class="button-primary submit" type="submit" value="Submit">
                            <!-- <div class="formResponse"></div> -->
                            <div class="fieldResponse"></div>
                        </fieldset>
                    </form>
                </div>  
            </div> <!-- end maintextbg -->

            <!-- Footer -->
    <footer class="myfooter">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    <h4><strong>Footer Info</strong>
                    </h4>
                    <p>123 Street Name<br>City, State 01234</p>
                </div>
            </div>
        </div>
    </footer>
        </div>
    </header>



    <!-- jQuery -->
    <script src="http://104.236.35.255/wp-content/themes/spicer-dev/coming-soon/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="http://104.236.35.255/wp-content/themes/spicer-dev/coming-soon/js/bootstrap.min.js"></script>
    <!-- Custom Theme JavaScript -->
        <!-- <script src="/coming-soon/js/jquery.js"></script> -->
        <!-- <script src="http://45.55.196.247/wp-content/themes/cement-league-dev/machines/libraries/backstretch/jquery.backstretch.min.js"></script> -->
        <script src="http://104.236.35.255/wp-content/themes/spicer-dev/machines/libraries/validate/validate.js"></script>
        <script>
            $(document).ready(function () {
                preventSent = false;
                // $.backstretch("/coming-soon/images/east_river-sm.jpg");
                processForm($('form'));
            });
            
            function processForm(theForm, doCaptcha){

        doCaptcha = (typeof doCaptcha !== "undefined") ? doCaptcha : false;
        csvUpdate = (theForm.data('csvupdate') != "") ? theForm.data('csvupdate') : 'formData';

        // you must enter your public key here - https://www.google.com/recaptcha/
        if(doCaptcha){
            Recaptcha.create("6LcgYfASAAAAAAvTvfb5r97NKTNnMbnCUnVN2-bp", theForm.find('.captchaField').attr('id'), {
                tabindex: 1,
                theme: "clean",
                callback: Recaptcha.focus_response_field
            });
        }

        function sendForm(){
            if(preventSent){
                return false;
            }
            $.post("handlers/saveContact.php", { 
                formData: formData, csvUpdate: csvUpdate 
            }, function(data) {
                if(data == "success"){
                    preventSent = true;
                    // Shadowbox.teardown(); 
                    // Shadowbox.init();

                    // // to add a message that can be customized on backend, change defaultPage below to match the page slug
                    // returnPageData(defaultPage).done(function(data1){
                    //  Shadowbox.open({
                    //      content: data1,
                    //      player: "html",
                    //      initialHeight: 500,
                    //      initialWidth: 500,
                    //      options: {onFinish: function () {
                    //          $('#sb-player').html("Thanks for Signing Up!")
                    //          $('#sb-player').append("<p class='printThis'>Print This</p>");
                    //          $('#sb-player').append("<p>title: " + theForm.find('.title').val() + "</p>");
                    //          $('#sb-player').append("<p>Telephone: " + theForm.find('.telephone').val() + "</p>");
                    //          $('#sb-player').append("<p>E-Mail: " + theForm.find('.email').val() + "</p>");
                    //          $('#sb-player').append("<p>Sample Select: " + theForm.find('.sample_select').val() + "</p>");

                    //          $('.printThis').on('click', function(){
                    //              window.print();
                    //          });
                    //      }} 
                    //  });
                    // });

                    $(".fieldResponse").html("<label>Form sent!</label>");
                    // $(".fieldResponse").css('color', 'green');
                    $(".fieldResponse").css('opacity', '1');
//                  $('#contactCaptcha').find('.fieldResponse').html('');
                } else {
                    $(".fieldResponse").html("Form not sent. Please refresh the page and try again");
//                  $(".fieldResponse").css('color', 'red');
                    // $(".fieldResponse").css('display', 'block !important');
                    $(".fieldResponse").css('opacity', '1');
                }
            });
        }
    

        theForm.validate({
            errorClass: "formError",
            submitHandler: function(thisForm) {
                formData = theForm.serialize();
                postArgs = {
                    challenge: $('#recaptcha_challenge_field').val(),
                    response: $('#recaptcha_response_field').val()              
                }

                // you must enter your private key in "/machines/libraries/recaptcha/recaptchaResponse.php" - https://www.google.com/recaptcha/
                if(doCaptcha){
                    $.post(pageDir + "/machines/libraries/recaptcha/recaptchaResponse.php", postArgs, function(data){
                        if(data.substring(0,4) == "true"){
                            sendForm();
                        } else {
                            Recaptcha.reload()
                            $('.fieldResponse').html('Incorrect Captcha, please try again');
                            // returnObject = $('<div class="fieldResponse">Incorrect Captcha, please try again</div>');
                            // $('#contactCaptcha').append(returnObject)
                        }
                    })
                } else {
                    sendForm();
                }

            },
            invalidHandler: function(event, validator) {
                errors = validator.numberOfInvalids();
                console.log('show')
                if (errors) {
                    message = errors == 1 ? 'You missed 1 field.' : 'You missed ' + errors + ' fields.';
//                  $(".formResponse").html(message);
                    $(".fieldResponse").show();
                } else {
                    $(".fieldResponse").hide();
                }
            },
            
            success: function(){
                $('.fieldResponse').css('opacity', '0');
            },

            errorPlacement: function(error, element) {
                switch(element.attr("name")){
                    case "spam":
                        $('.fieldResponse').html(error[0].outerHTML)
                        $('.fieldResponse').css('opacity', '1');
                        // $("input[name='spam']").parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
                    break;
                    case "transportation":
                        $('.fieldResponse').html(error[0].outerHTML)
                        $('.fieldResponse').css('opacity', '1');
                        // $("input[name='transportation']").parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
                    break;
                    case "subscribe_email":
                        $('.fieldResponse').html(error[0].outerHTML)
                        $('.fieldResponse').css('opacity', '1');
                        // element.parents('fieldset').after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
                    break;
                    default:
                        $('.fieldResponse').html(error[0].outerHTML)
                        $('.fieldResponse').css('opacity', '1');
                        // element.parent().after("<div class='fieldResponse'>" + error[0].outerHTML + "</div>");
                }
            },
            
            messages: {
                email: {
                    email: "Your email address must be in the format of name@domain.com"
                },
                telephone: {
                    phoneUS: "Your phone number must be in the format of 212-555-1000"
                },
            },
            rules: {
                email: {
                    required: true,
                    email: true
                },
                telephone:{
                    required: false,
                    phoneUS: true
                },
                title:{
                    required: false
                },
                subscribe_email:{
                    required: true,
                    email: true
                }
            }
        });
    }
        </script> 
    
    <script>
    // Closes the sidebar menu
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Opens the sidebar menu
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Scrolls to the selected menu item on the page
    $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
    </script>

</body>

</html>
